##Turtles chat

***Turtles chat*** - a web service that communicates in a general chat in real time.

The web service consists of four web pages: authorization page, registration page, password recovery page by e-mail 
and chat page.

When entering the site, the user enters the authorization page and fills in the required fields "Login" and "Password".
If the data entered by the user is correct the user is redirected to the general chat page. If the user is not yet 
registered, he can go to the registration page. On the registration page, the user must complete all required fields in 
order to register. If the user has forgotten the password, he can recover it by e-mail. On the chat page, the user can 
send and receive real-time messages. Users see messages from all members in the general chat. The user sees a list of 
users with an online status display.
The front part of the web service is made in JavaScript. The server is written in the Java programming language. 
The design is made in the HTML hypertext markup language using Cascading Style Sheets (CSS).
The remote Amazon server acts as a server. User data, as well as the history of correspondence, 
are stored in a relational Postgre SQL database.

###Requests

####*Success requests*

     1)  Autorization succsess

        URL: /auth
        body: {
                "login": "userlogin",
                "password": "passWord77"
        }
        Response:
            Status-code: 200 OK

    2) Registration success

        URL: /reg
        body: {
                "login": "userlogin",
                "password": "passWord77",
                "confirmPassword": "passWord77",
                "email": "user@gmail.com",
                "phoneNumber": "+380666666666",
                "companyName": ""
        }
        Response:
            Status-code: 201 Created

    3) Recover success

        URL: /recover
            body: {
                    "email": "user@gmail.com"
            }
            Response:
                Status-code: 200 OK

####*Invalid requests*

*Authorization invalid requests*

    1)  Fields cannot be empty

        URL: /auth
        body: {
                "login": "",
                "password": "passWord77"
        }
        Response:
            Status-code: 400 Bad request

    2)  Fields cannot be null

        URL: /auth
        body: {
                "login": null,
                "password": "passWord77"
        }
        Response:
            Status-code: 400 Bad request

    3)  Invalid credentials

        URL: /auth
        body: {
                "login": "userslogin",
                "password": "wrongpass"
        }
        Response:
            Status-code: 400 Bad request 

*Registration invalid requests*

    1)  Fields cannot be empty
    
        URL: /reg
        body: {
                "login": "userlogin",
                "password": "passWord77",
                "confirmPassword": "passWord77",
                "email": "",
                "phoneNumber": "",
                "companyName": "company"
        }
        Response:
            Status-code: 400 Bad request
            
    2)  Filled cannot be null
    
        URL: /reg
        body: {
                "login": "userlogin",
                "password": "passWord77",
                "confirmPassword": "passWord77",
                "email": null,
                "phoneNumber": "+380666666666",
                "companyName": "company"
        }
        Response:
            Status-code: 400 Bad request

    3)  Incorrect login
    
        URL: /reg
        body: {
                "login": "user_login",
                "password": "passWord77",
                "confirmPassword": "passWord77",
                "email": "user@gmail.com",
                "phoneNumber": "+380666666666",
                "companyName": ""
        }
        Response:
            Status-code: 400 Bad request

    4)  Incorrect password
    
        URL: /reg
        body: {
                "login": "userlogin",
                "password": "userpasswordddddddddddddddd",
                "confirmPassword": "userpasswordddddddddddddddd",
                "email": "user@gmail.com",
                "phoneNumber": "+380666666666",
                "companyName": ""
        }
        Response:
            Status-code: 400 Bad request

    5)  Password mismatch
    
        URL: /reg
        body: {
                "login": "userlogin"
                "password": "123456789Qwe"
                "confirmPassword": "Qwe123456789"
                "email": "user@gmail.com"
                "phoneNumber": "+380666666666"
                "companyName": ""
        }
        Response:
            Status-code: 400 Bad request

    6)  Incorrect e-mail
    
        URL: /reg
        body: {
                "login": "userlogin"
                "password": "passWord77",
                "confirmPassword": "passWord77",
                "email": "usergmail.com"
                "phoneNumber": "+380666666666"
                "companyName": ""
        }
        Response:
            Status-code: 400 Bad request

    7)  Incorrect number
    
        URL: /reg
        body: {
                "login": "userlogin"
                "password": "passWord77",
                "confirmPassword": "passWord77",
                "email": "user@gmail.com"
                "phoneNumber": "+380666"
                "companyName": ""
        }
        Response:
            Status-code: 400 Bad request

    8)  Exceeded number of characters
   
        URL: /reg
        body: {
                "login": "userlogin",
                "password": "passWord77",
                "confirmPassword": "passWord77",
                "email": "user@gmail.com",
                "phoneNumber": "+380666666666",
                "companyName": "texttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttexttext"
        }
        Response:
            Status-code: 400 Bad request

*Recovery password by e-mail invalid*

    1)  Fields cannot be empty

        URL: /recover
        body: {
                "email": ""
        }
        Response:
            Status-code: 400 Bad request

    2)  Incorrect email

        URL: /recover
        body: {
                "email": "user.com"
        }
        Response:
            Status-code: 400 Bad request

    3)  User doesn't exist

        URL: /recover
        body: {
                "email": "noregistration@gmail.com"
        }
        Response:
            Status-code: 400 Bad request
            
    4)  Fields cannot be null

        URL: /recover
        body: {
                "email": null
        }
        Response:
            Status-code: 400 Bad request

###Database

We use MySQL database as data storage.

*The database has the following tables and fields:*
+ Table “users” (this table stores data about the chat user)
  + field “id_user” (Int, Primary Key, Not Null, Auto Increment) 
  + field “login” (Varchar(16), Not Null)
  + field “email” (Varchar(45), Not Null)
  + field “password” (Varchar(16), Not NulL)
  + field “phone_number” (Varchar(15), Not Null)
  + field “company_name” (Varchar(80))
  + field “verified_email” (Boolean)
  + field “image” (Blob)
+ Table “chats” (this table stores chats that have users)
  + field “id_chat” (Int, Primary Key, Not Null, Auto Increment)
  + field “type_of_chat” (Varchar (10), Not Null)
+ Table "messages" (this table stores data about the message and the text of this message)
  + field “id_message” (Int, Primary Key, Not Null, Auto Increment)
  + field “owner_of_message” (Int, Foreign Key, Not Null)
  + field “time” (DataTime, Not Null)
  + field “text_message” (Varchar(300), Not Null)
  + field “id_chat” (Int, Foreign Key, Not Null)

*Relationships between tables:*
+ Table “users” is related to the “chats” table in a “many-to-many” relationship (there is an adjacent table "users_chats")
+ Table “chats” is related to the “messages” table in a «one-to-many» relationship
+ Table “users” is related to the “messages” table in a «one-to-many» relationship

*Table Schema:*

![img.png](img.png)

