-- MySQL Script generated by MySQL Workbench
-- Sun Feb 20 19:29:49 2022
-- Model: Sakila Full    Version: 2.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema turtlechat
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema turtlechat
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `turtlechat` ;
USE `turtlechat` ;

-- -----------------------------------------------------
-- Table `turtlechat`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `turtlechat`.`users` (
                                                    `id_user` INT NOT NULL AUTO_INCREMENT,
                                                    `login` VARCHAR(16) NOT NULL,
    `email` VARCHAR(45) NOT NULL,
    `password` VARCHAR(120) NOT NULL,
    `phone_number` VARCHAR(15) NOT NULL,
    `company_name` VARCHAR(80) NULL,
    `newPassword` VARCHAR(120) NULL,
    `link` VARCHAR(60) NULL,
    PRIMARY KEY (`id_user`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `turtlechat`.`chats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `turtlechat`.`chats` (
                                                    `id_chat` INT NOT NULL AUTO_INCREMENT,
                                                    `user1` INT NOT NULL,
                                                    `user2` INT NOT NULL,
                                                    PRIMARY KEY (`id_chat`),
    INDEX `fk_chats_users1_idx` (`user1` ASC),
    INDEX `fk_chats_users2_idx` (`user2` ASC),
    CONSTRAINT `fk_chats_users1`
    FOREIGN KEY (`user1`)
    REFERENCES `turtlechat`.`users` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_chats_users2`
    FOREIGN KEY (`user2`)
    REFERENCES `turtlechat`.`users` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `turtlechat`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `turtlechat`.`messages` (
                                                       `id_message` INT NOT NULL AUTO_INCREMENT,
                                                       `owner_of_message` INT NOT NULL,
                                                       `time` DATETIME NOT NULL,
                                                       `text_message` VARCHAR(300) NOT NULL,
    `id_chat` INT NULL,
    PRIMARY KEY (`id_message`),
    INDEX `fk_messages_users1_idx` (`owner_of_message` ASC),
    INDEX `fk_messages_chats1_idx` (`id_chat` ASC),
    CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`owner_of_message`)
    REFERENCES `turtlechat`.`users` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_messages_chats1`
    FOREIGN KEY (`id_chat`)
    REFERENCES `turtlechat`.`chats` (`id_chat`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
