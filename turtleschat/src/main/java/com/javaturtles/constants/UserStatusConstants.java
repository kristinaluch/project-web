package com.javaturtles.constants;

public class UserStatusConstants {
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";
}
