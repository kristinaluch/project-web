package com.javaturtles.constants;

public class Regex {

    private Regex() {
    }

    public static final String REGEX_CORRECT_PASSWORD = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{8,16}$";
    public static final String REGEX_CORRECT_LOGIN = "[A-Za-z0-9]{8,16}";
    public static final String REGEX_CORRECT_EMAIL = "^[a-zA-Z0-9+_\\.-]{1,32}+@[a-zA-Z0-9-]{1,32}\\.[a-zA-Z0-9-]{2,6}+$";
    public static final String REGEX_CORRECT_PHONE_NUMBER = "\\+[1-9]\\d{7,14}";
    public static final int COMPANY_NAME_MAX_SYMBOLS = 80;

}
