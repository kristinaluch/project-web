package com.javaturtles.constants;

public class ToUserTopicConstants {
    public static final String USERS_LIST = "users_list"; //"{"topic":"topic","payload":"{\"login2\":\"offline\",\"login1\":\"online\"}"}"
    public static final String ALL_CHAT_MESSAGE_TO_USER = "all_chat_message"; //"{"topic":"topic","payload":"[{\"login\":\"UserLogin1\",\"date\":\"Feb 18, 2022, 10:11:29 PM\",\"message\":\"Message 1\"},{\"login\":\"UserLogin2\",\"date\":\"Feb 18, 2022, 10:11:29 PM\",\"message\":\"Message 2\"}]"}"
    public static final String PRIVATE_CHAT_MESSAGE_TO_USER = "private_chat_message"; //"{"topic":"private_chat_message","payload":"{\"user\":\"userLogin\",\"receiver\":\"receiverLogin\",\"chatMessages\":[{\"login\":\"UserLogin1\",\"date\":\"Feb 18, 2022, 10:15:14 PM\",\"message\":\"Message 1\"},{\"login\":\"UserLogin2\",\"date\":\"Feb 18, 2022, 10:15:14 PM\",\"message\":\"Message 2\"}]}"}"
    public static final String ALL_CHAT_HISTORY = "all_chat_history"; //"{"topic":"all_chat_history","payload":"{\"userLogin\":\"UserLogin\",\"messageDate\":\"Feb 18, 2022, 9:59:59 PM\",\"message\":\"New message to u\"}"}"
    public static final String PRIVATE_CHAT_HISTORY = "private_chat_history"; //{"topic":"private_chat_history","payload":"{\"userLogin\":\"UserLogin\",\"receiverLogin\":\"ReceiverLogin\",\"messageDate\":\"Feb 18, 2022, 9:58:42 PM\",\"message\":\"New message to u\"}"}
    public static final String USER_CONNECTION = "user_connection"; //"{"topic":"topic","payload":"login"}"
    public static final String USER_DISCONNECTION = "user_disconnection"; //"{"topic":"topic","payload":"login"}"
}
