package com.javaturtles.constants;

public class ConstantsForDB {

    public static final String SQL_INSERT_USER =
            "INSERT INTO users (login, email, password, phone_number, company_name, newPassword, link) VALUES (?, ?, ?, ?, ?, ?, ?);";

    public static final String SQL_SELECT_ALL_USERS =
            "SELECT * FROM turtlechat.users;";

    public static final String SQL_SELECT_ALL_GLOBAL_CHAT_MESSAGES =
            "SELECT login, time, text_message, id_chat FROM turtlechat.messages JOIN turtlechat.users ON messages.owner_of_message = users.id_user WHERE id_chat IS null;";

    public static final String SQL_INSERT_MESSAGE =
            "INSERT INTO messages (owner_of_message, time, text_message, id_chat) VALUES (?, ?, ?, ?);";

    public static final String SQL_SELECT_ALL_CHATS =
            "SELECT * FROM turtlechat.chats;";

    public static final String SQL_INSERT_PRIVATE_CHAT =
            "INSERT INTO chats (user1, user2) VALUES (?, ?);";

    public static final String SQL_SELECT_PRIVATE_MESSAGES =
            "SELECT login, time, text_message, id_chat FROM turtlechat.messages JOIN turtlechat.users ON messages.owner_of_message = users.id_user WHERE id_chat = ?;";

    public static final String SQL_SET_LINK_AND_PASS =
            "UPDATE users SET newPassword = ?, link = ? WHERE id_user = ?;";

    public static final String SQL_SET_NEW_PASS =
            "UPDATE turtlechat.users SET password = ? WHERE id_user = ?;";

    public static final String SQL_SELECT_LINK_AND_PASS =
            "SELECT * FROM turtlechat.users WHERE link = ?;";

    private ConstantsForDB() {}
}
