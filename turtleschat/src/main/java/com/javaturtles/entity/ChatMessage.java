package com.javaturtles.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class ChatMessage implements Serializable {
    private String login;
    private Date date;
    private String message;

    public ChatMessage(String login, Date date, String message) {
        this.login = login;
        this.date = date;
        this.message = message;
    }

    public ChatMessage() {
    }

    public String getLogin() {
        return login;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatMessage that = (ChatMessage) o;
        return Objects.equals(login, that.login) && Objects.equals(date, that.date) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, date, message);
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "login='" + login + '\'' +
                ", date=" + date +
                ", message='" + message + '\'' +
                '}';
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
