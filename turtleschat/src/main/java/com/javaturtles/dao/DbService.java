package com.javaturtles.dao;
import com.javaturtles.constants.ConstantsForDB;
import com.javaturtles.dbutil.PropertyFileService;
import com.javaturtles.entity.ChatMessage;
import com.javaturtles.model.User;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DbService implements IDbService {

    private static final Logger log = Logger.getLogger(DbService.class);
    private final PropertyFileService property = new PropertyFileService();
    private Connection dbCon = null;

    @Override
    public boolean addUser(User user) {
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_INSERT_USER))
        {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getPhoneNumber());
            preparedStatement.setString(5, user.getCompany());
            preparedStatement.setString(6, null);
            preparedStatement.setString(7, null);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
            return false;
        }
        return true;
    }

    @Override
    public boolean isNotUniqueLogin(String login) {
        boolean findLoginInDB = false;
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement())
        {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);
            while (result.next()) {
                if (result.getString("login").equals(login))
                {
                    findLoginInDB = true;
                }
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return findLoginInDB;
    }

    @Override
    public boolean isNotUniqueEmail(String email) {
        boolean findEmailInDB = false;
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement())
        {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);
            while (result.next()) {
                if (result.getString("email").equals(email))
                {
                    findEmailInDB = true;
                }
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return findEmailInDB;
    }

    @Override
    public boolean confirmUserByLogin(String login, String password) {
        boolean authorization = false;
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement()) {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);
            while (result.next()) {
                if (result.getString("login").equals(login)
                        && result.getString("password").equals(password))
                {
                    authorization = true;
                }
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return authorization;
    }

    @Override
    public boolean confirmUserByEmail(String email, String password) {
        boolean authorization = false;
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement()) {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);
            while (result.next()) {
                if (result.getString("email").equals(email)
                        && result.getString("password").equals(password))
                {
                    authorization = true;
                }
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return authorization;
    }

    @Override
    public boolean setLinkAndNewPassword(String login, String link, String newPassword) {
        boolean set = false;
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_SET_LINK_AND_PASS))
        {
            preparedStatement.setString(1, newPassword);
            preparedStatement.setString(2, link);
            preparedStatement.setInt(3,getUserID(login));
            preparedStatement.executeUpdate();
            set = true;
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return set;
    }

    @Override
    public boolean updatePassword(String link) {
        String newPassword = getNewPassword(link);
        boolean set = false;
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_SET_NEW_PASS))
        {
            preparedStatement.setString(1, newPassword);
            preparedStatement.setInt(2, getUserIDOfLink(link));
            preparedStatement.executeUpdate();
            set = true;
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return set;
    }

    @Override
    public List<String> getUsersList() {
        List<String> usersLogins = new ArrayList<>();
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement()) {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);
            while (result.next()) {
                String userLogin = result.getString("login");
                usersLogins.add(userLogin);
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return usersLogins;
    }

    @Override
    public List<ChatMessage> getAllChatHistory() {
        List<ChatMessage> messages = new ArrayList<>();
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement()) {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_GLOBAL_CHAT_MESSAGES);
            while (result.next()) {
                ChatMessage message = new ChatMessage();
                message.setLogin(result.getString("login"));
                message.setDate(result.getDate("time"));// возможна ошибка
                message.setMessage(result.getString("text_message"));
                messages.add(message);
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return messages;
    }

    @Override
    public List<ChatMessage> getPrivateChatHistory(String login1, String login2) {
        if (!checkPrivateChat(login1, login2))
        {
            createPrivateChat(login1, login2);
        }
        int id_chat = getIDPrivateChat(login1, login2);
        List<ChatMessage> messages = new ArrayList<>();
        ResultSet result;
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_SELECT_PRIVATE_MESSAGES)) {
            preparedStatement.setInt(1, id_chat);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                ChatMessage message = new ChatMessage();
                message.setLogin(result.getString("login"));
                message.setDate(result.getDate("time"));// возможна ошибка
                message.setMessage(result.getString("text_message"));
                messages.add(message);
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return messages;
    }

    @Override
    public boolean addAllMessage(String login, Date date, String message) {
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_INSERT_MESSAGE))
        {
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            preparedStatement.setInt(1, getUserID(login));
            preparedStatement.setDate(2, sqlDate);
            preparedStatement.setString(3, message);
            preparedStatement.setString(4, null);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
            return false;
        }
        return true;
    }

    @Override
    public boolean addPrivateMessage(String login, String recipient, Date date, String message) {
        if (!checkPrivateChat(login, recipient))
        {
            createPrivateChat(login, recipient);
        }
        int ownerOfMessage = getUserID(login);
        int id_chat = getIDPrivateChat(login, recipient);
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_INSERT_MESSAGE))
        {
            java.sql.Date sqlDate = new java.sql.Date(date.getTime());
            preparedStatement.setInt(1, ownerOfMessage);
            preparedStatement.setDate(2, sqlDate);
            preparedStatement.setString(3, message);
            preparedStatement.setInt(4, id_chat);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
            return false;
        }
        return true;
    }

    private int getUserID (String login) {
        int id = 0;
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement()) {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);
            while (result.next()) {
                if (login.equals(result.getString("login"))) {
                    id = result.getInt("id_user");
                }
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return id;
    }

    private int getIDPrivateChat (String login1, String login2) {
        int id = 0;
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement()) {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_CHATS);
            while (result.next()) {
                if (getUserID(login1) == result.getInt("user1")
                        && getUserID(login2) == result.getInt("user2")) {
                    id = result.getInt("id_chat");
                }
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return id;
    }

    private boolean checkPrivateChat (String login1, String login2) {
        boolean confirm = false;
        ResultSet result;
        try (Statement statement = getDBConnection().createStatement()) {
            result = statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_CHATS);
            while (result.next()) {
                if (getUserID(login1) == result.getInt("user1")
                        && getUserID(login2) == result.getInt("user2")) {
                    confirm = true;
                }
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return confirm;
    }

    private void createPrivateChat (String login1, String login2) {
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_INSERT_PRIVATE_CHAT))
        {
            preparedStatement.setInt(1, getUserID(login1));
            preparedStatement.setInt(2, getUserID(login2));
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
    }

    private String getNewPassword(String link) {
        String newPassword = null;
        ResultSet result;
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_SELECT_LINK_AND_PASS)) {
            preparedStatement.setString(1, link);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                newPassword = result.getString("newPassword");
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return newPassword;
    }

    private int getUserIDOfLink(String link) {
        int id=0;
        ResultSet result;
        try (PreparedStatement preparedStatement = getDBConnection().prepareStatement(ConstantsForDB.SQL_SELECT_LINK_AND_PASS)) {
            preparedStatement.setString(1, link);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                id = result.getInt("id_user");
            }
            result.close();
        } catch (SQLException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        }
        return id;
    }

    private Connection getDBConnection() {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            dbCon = DriverManager.getConnection(property.getDBHostFromProperty(),
                    property.getDBLoginFromProperty(), property.getDBPasswordFromProperty());
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage() + "in" + DbService.class.getName());
        } catch (SQLException throwables) {
            log.error(throwables.getMessage() + "in" + DbService.class.getName());
        }
        return dbCon;
    }

}
