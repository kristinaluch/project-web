package com.javaturtles.util.validation;

import com.javaturtles.dao.IDbService;
import com.javaturtles.dto.AuthorizationDto;
import com.javaturtles.dto.RegistrationDto;
import com.javaturtles.exception.ValidateException;
import com.javaturtles.passwordencoder.PasswordEncoder;
import org.apache.log4j.Logger;
import java.util.regex.Pattern;

import static com.javaturtles.constants.Regex.*;
import static com.javaturtles.constants.ResponseMessage.*;
import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;
import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;

public class Validation {


    private static final Logger log = Logger.getLogger(Validation.class);
    private final IDbService dataBase;
    private final PasswordEncoder passwordEncoder;

    public Validation(IDbService dataBase, PasswordEncoder passwordEncoder) {
        this.dataBase = dataBase;
        this.passwordEncoder = passwordEncoder;
    }

    public static final int MESSAGE_LENGTH_MAX = 300;


    public String getValidateResponse(FromUserAllChatMessage fromUserAllChatMessage) {
        try {
            isMessageDataNull(fromUserAllChatMessage);
            isCorrectLogin(fromUserAllChatMessage.getUserLogin());
            isCorrectMessage(fromUserAllChatMessage.getMessage());
            return MSG_SUCCESS;
        } catch (ValidateException e) {
            log.warn(e.getMessage());
            return e.getMessage();
        }
    }

    private boolean isMessageDataNull(FromUserAllChatMessage msg) throws ValidateException {
        boolean nullEmptyFields = msg.getUserLogin() == null || msg.getUserLogin().isBlank()
                || msg.getMessage() == null || msg.getMessage().isBlank();
        if (nullEmptyFields) {
            throw new ValidateException(MSG_INVALID_EMPTY_NULL_FIELD);
        }
        return true;
    }

    public String getValidateResponse(RegistrationDto regDTO) {
        if (regDTO == null) {
            return MSG_INVALID_NULL;
        }
        try {
            areFieldsNotEmpty(regDTO);
            isCorrectLogin(regDTO.getLogin());
            isCorrectPassword(regDTO.getPassword());
            isPasswordConfirm(regDTO.getPassword(), regDTO.getConfirmPassword());
            isCorrectEmail(regDTO.getEmail());
            isCorrectPhoneNumber(regDTO.getPhoneNumber());
            isCorrectCompanyName(regDTO.getCompany());
            isUniqueLogin(regDTO.getLogin());
            isUniqueEmail(regDTO.getEmail());
            return MSG_SUCCESS;
        } catch (ValidateException e) {
            log.warn(e.getMessage());
            return e.getMessage();
        }
    }

    public String getValidateResponse(AuthorizationDto authDto) {
        if (authDto == null) {
            return MSG_INVALID_NULL;
        }
        try {
            areFieldsNotEmpty(authDto);
            if (authDto.getLogin().contains("@")) {
                isCorrectEmailCredentials(authDto);
            } else {
                isCorrectLoginCredentials(authDto);
            }
            return MSG_SUCCESS;
        } catch (ValidateException e) {
            log.warn(e.getMessage());
            return e.getMessage();
        }
    }

    public String getValidateResponse(FromUserPrivateChatMessage fromUserPrivateChatMessage) {
        try {
            isMessageDataNull(fromUserPrivateChatMessage);
            isCorrectLogin(fromUserPrivateChatMessage.getUserLogin());
            isCorrectLogin( fromUserPrivateChatMessage.getReceiverLogin());
            isCorrectMessage(fromUserPrivateChatMessage.getMessage());
            return MSG_SUCCESS;
        } catch (ValidateException e) {
            log.warn(e.getMessage());
            return e.getMessage();
        }

    }

    public String getLoginValidateResponse(String login) {
        try {
            isCorrectLogin(login);
            return MSG_SUCCESS;
        } catch (ValidateException e) {
            log.warn(e.getMessage());
            return e.getMessage();
        }
    }

    private boolean isCorrectLoginCredentials(AuthorizationDto authDto) throws ValidateException {
        String pass = passwordEncoder.getHash(authDto.getPassword());
        isCorrectPassword(authDto.getPassword());
        isCorrectLogin(authDto.getLogin());
        if (!dataBase.confirmUserByLogin(authDto.getLogin(), pass)) {
            throw new ValidateException(MSG_INVALID_CREDENTIALS);
        }
        return true;
    }

    private boolean isCorrectEmailCredentials(AuthorizationDto authDto) throws ValidateException {
        String pass = passwordEncoder.getHash(authDto.getPassword());
        isCorrectPassword(authDto.getPassword());
        try {
            isCorrectEmail(authDto.getLogin());
        } catch (ValidateException e) {
            throw new ValidateException(MSG_INVALID_INCORRECT_LOGIN);
        }
        if (!dataBase.confirmUserByEmail(authDto.getLogin(), pass)) {
            throw new ValidateException(MSG_INVALID_CREDENTIALS);
        }
        return true;
    }

    private boolean areFieldsNotEmpty(RegistrationDto regDto) throws ValidateException {
        String [] fields = new String[]{regDto.getLogin(), regDto.getPassword(), regDto.getConfirmPassword(),
                regDto.getEmail(), regDto.getPhoneNumber()};
        for (String field : fields) {
            if (field == null || field.isBlank()) {
                throw new ValidateException(MSG_INVALID_EMPTY_NULL_FIELD);
            }
        }
        return true;
    }

    private boolean areFieldsNotEmpty(AuthorizationDto authDto) throws ValidateException {
        boolean nullEmptyFields = authDto.getLogin() == null || authDto.getLogin().isBlank()
                || authDto.getPassword() == null || authDto.getPassword().isBlank();
        if (nullEmptyFields) {
            throw new ValidateException(MSG_INVALID_EMPTY_NULL_FIELD);
        }
        return true;
    }

    private boolean isCorrect(String checkField, String regex) {
        return Pattern.compile(regex).matcher(checkField).matches();
    }

    private boolean isCorrectLogin(String login) throws ValidateException {
        if (!isCorrect(login, REGEX_CORRECT_LOGIN)) {
            throw new ValidateException(MSG_INVALID_INCORRECT_LOGIN);
        }
        return true;
    }

    private boolean isUniqueLogin(String login) throws ValidateException {
     if (dataBase.isNotUniqueLogin(login)) {
         throw new ValidateException(MSG_INVALID_INCORRECT_LOGIN);
     }
        return true;
    }

    private boolean isCorrectPassword(String password) throws ValidateException {
        if (!isCorrect(password, REGEX_CORRECT_PASSWORD)) {
            throw new ValidateException(MSG_INVALID_INCORRECT_PASSWORD);
        }
        return true;
    }

    private boolean isPasswordConfirm(String password, String confirmPassword) throws ValidateException {
        if (!password.equals(confirmPassword)) {
            throw new ValidateException(MSG_INVALID_CONFIRM_PASSWORD_MISMATCH);
        }
        return true;
    }

    private boolean isCorrectEmail(String email) throws ValidateException {
        if (!isCorrect(email, REGEX_CORRECT_EMAIL)) {
            throw new ValidateException(MSG_INVALID_INCORRECT_EMAIL);
        }
        return true;
    }

    private boolean isUniqueEmail(String email) throws ValidateException {
        if (dataBase.isNotUniqueEmail(email)) {
            throw new ValidateException(MSG_INVALID_INCORRECT_EMAIL);
        }
        return true;
    }

    private boolean isExistEmail(String email) throws ValidateException{
        if (dataBase.isNotUniqueEmail(email)) {
            throw new ValidateException(MSG_INVALID_INCORRECT_EMAIL);
        }
        return true;
    }

    private boolean isCorrectPhoneNumber(String phoneNumber) throws ValidateException {
        if (!isCorrect(phoneNumber, REGEX_CORRECT_PHONE_NUMBER)) {
            throw new ValidateException(MSG_INVALID_INCORRECT_NUMBER);
        }
        return true;
    }

    private boolean isCorrectCompanyName(String companyName) throws ValidateException {
        if (companyName == null){
            return true;
        }
        if (companyName.length() > COMPANY_NAME_MAX_SYMBOLS) {
            throw new ValidateException(MSG_INVALID_EXCEEDED_CHARACTERS);
        }
        return true;
    }

    private boolean isMessageDataNull(FromUserPrivateChatMessage msg) throws ValidateException {
        boolean nullEmptyFields = msg.getUserLogin() == null || msg.getUserLogin().isBlank()
                || msg.getReceiverLogin() == null || msg.getReceiverLogin().isBlank()
        || msg.getMessage() == null || msg.getMessage().isBlank();
        if (nullEmptyFields) {
            throw new ValidateException(MSG_INVALID_EMPTY_NULL_FIELD);
        }
        return true;
    }

    private boolean isCorrectMessage(String msg) throws ValidateException {
        if (msg.length() > MESSAGE_LENGTH_MAX) {
            throw new ValidateException(MSG_INVALID_EXCEEDED_CHARACTERS);
        }
        return true;
    }

}
