package com.javaturtles.util.uuid;

import java.util.UUID;

public class LinkIdCreator {

    public String getUUID(){
        UUID uniqueKey = UUID.randomUUID();
        String uuidStr = uniqueKey.toString();
        return uuidStr;
    }
}
