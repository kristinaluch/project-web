package com.javaturtles.util.message_handler;

import com.javaturtles.util.session_handler.ChatSessionsHandler;

import static com.javaturtles.constants.FromUserTopicConstants.ALL_CHAT_HISTORY_REQUEST;

public class AllChatHistoryRequestHandler implements MessageHandler {
    private ChatSessionsHandler chatSessionsHandler;

    public AllChatHistoryRequestHandler(ChatSessionsHandler chatSessionsHandler) {
        this.chatSessionsHandler = chatSessionsHandler;
    }

    @Override
    public String getMessageRequest() {
        return ALL_CHAT_HISTORY_REQUEST;
    }

    @Override
    public void process(String message, String login) {
        chatSessionsHandler.sendAllChatHistory(login);
    }
}
