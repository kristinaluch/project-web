package com.javaturtles.util.message_handler;

import com.javaturtles.util.session_handler.ChatSessionsHandler;
import com.javaturtles.util.validation.Validation;
import com.javaturtles.websocket.ChatWebsocket;
import org.apache.log4j.Logger;

import static com.javaturtles.constants.FromUserTopicConstants.PRIVATE_CHAT_HISTORY_REQUEST;
import static com.javaturtles.constants.ResponseMessage.MSG_SUCCESS;

public class PrivateChatHistoryRequestHandler implements MessageHandler {
    private static final Logger log = Logger.getLogger(ChatWebsocket.class);
    private ChatSessionsHandler chatSessionsHandler;
    private Validation validation;

    public PrivateChatHistoryRequestHandler(ChatSessionsHandler chatSessionsHandler, Validation validation) {
        this.chatSessionsHandler = chatSessionsHandler;
        this.validation = validation;
    }

    @Override
    public String getMessageRequest() {
        return PRIVATE_CHAT_HISTORY_REQUEST;
    }

    @Override
    public void process(String message, String login) {
        String validationResult = validation.getLoginValidateResponse(login);
        if (!validationResult.equals(MSG_SUCCESS)) {
            log.error(validationResult);
            return;
        }
        chatSessionsHandler.sendPrivateChatHistory(login, message);
    }
}
