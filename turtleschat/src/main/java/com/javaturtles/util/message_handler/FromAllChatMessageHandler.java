package com.javaturtles.util.message_handler;

import com.google.gson.Gson;
import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;
import com.javaturtles.util.session_handler.ChatSessionsHandler;
import com.javaturtles.util.validation.Validation;
import com.javaturtles.websocket.ChatWebsocket;
import org.apache.log4j.Logger;
import static com.javaturtles.constants.FromUserTopicConstants.ALL_CHAT_MESSAGE_FROM_USER;
import static com.javaturtles.constants.ResponseMessage.MSG_SUCCESS;

public class FromAllChatMessageHandler implements MessageHandler {
    private static final Logger log = Logger.getLogger(ChatWebsocket.class);
    private ChatSessionsHandler chatSessionsHandler;
    private Validation validation;
    private Gson gson;

    public FromAllChatMessageHandler(ChatSessionsHandler chatSessionsHandler, Validation validation, Gson gson) {
        this.chatSessionsHandler = chatSessionsHandler;
        this.validation = validation;
        this.gson = gson;
    }

    @Override
    public String getMessageRequest() {
        return ALL_CHAT_MESSAGE_FROM_USER;
    }

    @Override
    public void process(String message, String login) {
        FromUserAllChatMessage fromUserAllChatMessage = gson.fromJson(message, FromUserAllChatMessage.class);
        String validationResult = validation.getValidateResponse(fromUserAllChatMessage);
        if (!validationResult.equals(MSG_SUCCESS)) {
            log.error(validationResult);
            return;
        }
        System.out.println(fromUserAllChatMessage);
        chatSessionsHandler.sendAllChatMessage(fromUserAllChatMessage);
    }
}
