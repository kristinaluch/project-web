package com.javaturtles.util.session_handler;

import com.javaturtles.cache.UserSessionCache;
import com.javaturtles.dao.IDbService;
import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;
import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;
import com.javaturtles.dto.message.to_user.PrivateChatHistoryMessage;
import com.javaturtles.dto.message.to_user.ToUserChatMessage;
import com.javaturtles.entity.ChatMessage;
import com.javaturtles.util.MessageConverter;
import com.javaturtles.util.TimeUtil;
import com.javaturtles.websocket.ChatWebsocket;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import static com.javaturtles.constants.ToUserTopicConstants.*;
import static com.javaturtles.constants.UserStatusConstants.OFFLINE;
import static com.javaturtles.constants.UserStatusConstants.ONLINE;

public class ChatSessionsHandler {
    private static final Logger log = Logger.getLogger(ChatWebsocket.class);
    private MessageConverter messageConverter;
    private UserSessionCache userSessionCache;
    private IDbService dbService;

    public ChatSessionsHandler(MessageConverter messageConverter, UserSessionCache userSessionCache,
                               IDbService dbService) {
        this.messageConverter = messageConverter;
        this.userSessionCache = userSessionCache;
        this.dbService = dbService;
    }

    public void sendUsersList(String login) {
        Session session = userSessionCache.getSession(login);
        List<String> usersList = dbService.getUsersList();
        Set<String> onlineUsers = userSessionCache.getLoginSet();
        Map<String, String> users = usersList.stream()
                .filter(s -> !s.equals(login))
                .collect(Collectors.toMap(s -> s, s -> onlineUsers.contains(s) ? ONLINE : OFFLINE));
        String toUserMessage = messageConverter.createToUserMessage(USERS_LIST, users);
        try {
            session.getRemote().sendString(toUserMessage);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void sendAllChatHistory(String login) {
        Session session = userSessionCache.getSession(login);
        List<ChatMessage> chatMessages = dbService.getAllChatHistory();
        String toUserMessage = messageConverter.createToUserMessage(ALL_CHAT_HISTORY, chatMessages);
        try {
            session.getRemote().sendString(toUserMessage);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void sendPrivateChatHistory(String userLogin, String receiverLogin) {
        List<ChatMessage> chatMessages = dbService.getPrivateChatHistory(userLogin, receiverLogin);
        PrivateChatHistoryMessage privateChatHistoryMessage =
                new PrivateChatHistoryMessage(userLogin, receiverLogin, chatMessages);
        String toUserMessage = messageConverter.createToUserMessage(PRIVATE_CHAT_HISTORY, privateChatHistoryMessage);
        Session session = userSessionCache.getSession(userLogin);
        try {
            session.getRemote().sendString(toUserMessage);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void notifyUserConnected(String login) {
        String toUserMessage = messageConverter.createToUserMessage(USER_CONNECTION, login);
        sendMessageToEverybody(toUserMessage);
    }

    public void notifyUserDisconnected(String login) {
        String toUserMessage = messageConverter.createToUserMessage(USER_DISCONNECTION, login);
        sendMessageToEverybody(toUserMessage);
    }

    public void sendAllChatMessage(FromUserAllChatMessage fromUserAllChatMessage) {
        Date now = new Date(TimeUtil.getCurrentTimeMillis());
        ToUserChatMessage toUserAllChatMessage = new ToUserChatMessage(fromUserAllChatMessage.getUserLogin(),
                null, now, fromUserAllChatMessage.getMessage());
        System.out.println(toUserAllChatMessage);
        dbService.addAllMessage(toUserAllChatMessage.getUserLogin(), now, toUserAllChatMessage.getMessage());
        String toUserMessage = messageConverter.createToUserMessage(ALL_CHAT_MESSAGE_TO_USER, toUserAllChatMessage);
        sendMessageToEverybody(toUserMessage);
    }

    public void sendPrivateChatMessage(FromUserPrivateChatMessage fromUserPrivateChatMessage) {
        Date now = new Date(TimeUtil.getCurrentTimeMillis());
        ToUserChatMessage toUserChatMessage = new ToUserChatMessage(
                fromUserPrivateChatMessage.getUserLogin(),
                fromUserPrivateChatMessage.getReceiverLogin(),
                now,
                fromUserPrivateChatMessage.getMessage()
        );
        System.out.println(toUserChatMessage);
        dbService.addPrivateMessage(toUserChatMessage.getUserLogin(),
                toUserChatMessage.getReceiverLogin(),
                toUserChatMessage.getMessageDate(),
                toUserChatMessage.getMessage());
        String toUserMessage = messageConverter.createToUserMessage(PRIVATE_CHAT_MESSAGE_TO_USER, toUserChatMessage);
        Session user = userSessionCache.getSession(toUserChatMessage.getUserLogin());
        Session interlocutor = userSessionCache.getSession(toUserChatMessage.getReceiverLogin());
        try {
            if (user != null) {
                user.getRemote().sendString(toUserMessage);
            }
            if (interlocutor != null) {
                interlocutor.getRemote().sendString(toUserMessage);
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    private void sendMessageToEverybody(String message) {
        Set<String> onlineUsers =  userSessionCache.getLoginSet();
        onlineUsers.stream()
                .forEach(s -> {
                    try {
                        userSessionCache.getSession(s).getRemote().sendString(message);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }
}
