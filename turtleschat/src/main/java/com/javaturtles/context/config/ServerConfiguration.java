package com.javaturtles.context.config;

import com.google.gson.Gson;
import com.javaturtles.cache.UserSessionCache;
//import com.javaturtles.context.AppContext;
import com.javaturtles.controller.AuthorizationController;
import com.javaturtles.controller.RegistrationController;
import com.javaturtles.dao.DbService;
import com.javaturtles.dao.IDbService;
import com.javaturtles.factory.MessageHandlerFactory;
import com.javaturtles.filter.CORSFilter;
import com.javaturtles.passwordencoder.PasswordEncoder;
import com.javaturtles.property.PropertiesUtil;
import com.javaturtles.service.AuthorizationService;
import com.javaturtles.service.RegistrationService;
import com.javaturtles.util.MessageConverter;
import com.javaturtles.util.jwt.JWTUtil;
import com.javaturtles.util.message_handler.*;
import com.javaturtles.util.session_handler.ChatSessionsHandler;
import com.javaturtles.util.validation.Validation;
import com.javaturtles.websocket.ChatWebsocket;
import com.javaturtles.websocket.ChatWebsocketServlet;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import javax.servlet.DispatcherType;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.javaturtles.constants.ConfigurationConstants.PORT;

public class ServerConfiguration {
    public static final Gson gson = new Gson();
    public static final PasswordEncoder passwordEncoder = new PasswordEncoder(PropertiesUtil.getInstance().getSalt());
    public static final IDbService dbService = new DbService();
    public static final Map<String, Session> userSessions = new ConcurrentHashMap<>();
    public static final UserSessionCache userSessionCache = new UserSessionCache(userSessions);
    public static final Validation validation = new Validation(dbService, passwordEncoder);
    public static final MessageConverter messageConverter = new MessageConverter(gson);
    public static final ChatSessionsHandler chatSessionsHandler = new ChatSessionsHandler(messageConverter,
            userSessionCache, dbService);
    public static final AllChatHistoryRequestHandler allChatHistoryRequestHandler =
            new AllChatHistoryRequestHandler(chatSessionsHandler);
    public static final PrivateChatHistoryRequestHandler privateChatHistoryRequestHandler =
            new PrivateChatHistoryRequestHandler(chatSessionsHandler, validation);
    public static final FromAllChatMessageHandler fromAllChatMessageHandler =
            new FromAllChatMessageHandler(chatSessionsHandler, validation, gson);
    public static final FromPrivateChatMessageHandler fromPrivateChatMessageHandler =
            new FromPrivateChatMessageHandler(chatSessionsHandler, validation, gson);
    public static final Set<MessageHandler> messageHandlers = new HashSet<>(List.of(allChatHistoryRequestHandler,
            privateChatHistoryRequestHandler, fromAllChatMessageHandler, fromPrivateChatMessageHandler));
    public static final MessageHandlerFactory messageHandlerFactory = new MessageHandlerFactory(messageHandlers);

    public Server buildServer(){
        ServletHolder websocketHolder = new ServletHolder("websocketHolder", ChatWebsocketServlet.class);
        JWTUtil jwtUtil = new JWTUtil();
        AuthorizationService authorizationService = new AuthorizationService(gson, validation, jwtUtil);
        AuthorizationController authorizationController = new AuthorizationController(authorizationService);
        RegistrationService registrationService = new RegistrationService(gson, validation, dbService, passwordEncoder);
        RegistrationController registrationController = new RegistrationController(registrationService);
//        пеккедж в котором криейтор, сервер конфигурации - вверху контекст
        ServletHolder regServletHolder = new ServletHolder("regHolder", registrationController);
        ServletHolder authServletHolder = new ServletHolder("authHolder", authorizationController);
        Server server = new Server();
        ServerConnector serverConnector = new ServerConnector(server);
        serverConnector.setPort(PORT);
        server.setConnectors(new ServerConnector[]{serverConnector});
        ServletContextHandler context = new ServletContextHandler();
        context.addServlet(authServletHolder, "/auth");
        context.addServlet(regServletHolder, "/reg");
        context.addServlet(websocketHolder, "/chat");
        context.addFilter(CORSFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
        HandlerCollection hc = new HandlerCollection();
        hc.setHandlers(new Handler[]{context});
        server.setHandler(hc);
        return server;
    }

    public static Gson getGson() {
        return gson;
    }

    public static ChatSessionsHandler getChatSessionsHandler() {
        return chatSessionsHandler;
    }

    public static UserSessionCache getUserSessionCache() {
        return userSessionCache;
    }

    public static MessageHandlerFactory getMessageHandlerFactory() {
        return messageHandlerFactory;
    }
}

