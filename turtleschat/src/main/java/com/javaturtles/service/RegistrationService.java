package com.javaturtles.service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.javaturtles.dao.IDbService;
import com.javaturtles.dto.RegistrationDto;
import com.javaturtles.dto.ResponseDto;
import com.javaturtles.passwordencoder.PasswordEncoder;
import com.javaturtles.util.validation.Validation;
import com.javaturtles.model.User;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletResponse;
import static com.javaturtles.constants.ResponseMessage.*;

public class RegistrationService {
    private static final Logger LOG = Logger.getLogger(RegistrationService.class);
    private final Gson gson;
    private final Validation validation;
    private final IDbService usersDbUtility;
    private final PasswordEncoder passwordEncoder;

    public RegistrationService(Gson gson, Validation validation, IDbService usersDbUtility, PasswordEncoder passwordEncoder) {
        this.gson = gson;
        this.validation = validation;
        this.usersDbUtility = usersDbUtility;
        this.passwordEncoder = passwordEncoder;
    }

    public ResponseDto registerUser(String body) {
        try {
            RegistrationDto registrationDTO = gson.fromJson(body, RegistrationDto.class);
            String validateResponse = validation.getValidateResponse(registrationDTO);
            if (!validateResponse.equals(MSG_SUCCESS)) {
                return new ResponseDto(validateResponse, HttpServletResponse.SC_BAD_REQUEST);
            }
            String hashPassword = passwordEncoder.getHash(registrationDTO.getPassword());
            User user = new User(registrationDTO.getLogin(), hashPassword, registrationDTO.getEmail(),
                    registrationDTO.getPhoneNumber(), registrationDTO.getCompany());
            usersDbUtility.addUser(user);
            return new ResponseDto(MSG_CREATED, HttpServletResponse.SC_CREATED);
        } catch (JsonSyntaxException e) {
            LOG.error(e.getMessage());
            return new ResponseDto(MSG_INCORRECT_DATA, HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
