package com.javaturtles.service;

import com.google.gson.Gson;
import com.javaturtles.dto.AuthorizationDto;
import com.javaturtles.dto.ResponseDto;
import com.javaturtles.util.jwt.JWTUtil;
import com.javaturtles.util.validation.Validation;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletResponse;
import static com.javaturtles.constants.ResponseMessage.MSG_INCORRECT_DATA;
import static com.javaturtles.constants.ResponseMessage.MSG_SUCCESS;

public class AuthorizationService {

    private static final Logger log = Logger.getLogger(RegistrationService.class);

    private final Gson gson;
    private final Validation validation;
    private final JWTUtil jwtUtil;
    private static final int TT_MILLIS = 4;

    public AuthorizationService(Gson gson, Validation validation, JWTUtil jwtUtil) {
        this.gson = gson;
        this.validation = validation;
        this.jwtUtil = jwtUtil;
    }

    public ResponseDto authorizeUser(String body) {

        AuthorizationDto authDTO;
        try {
            System.out.println(body);
            authDTO = gson.fromJson(body, AuthorizationDto.class);
            System.out.println(authDTO);
            String validateResponse = validation.getValidateResponse(authDTO);

            if (!validateResponse.equals(MSG_SUCCESS)) {
                return new ResponseDto(validateResponse, HttpServletResponse.SC_UNAUTHORIZED);
            } else {
                return new ResponseDto(jwtUtil.createJWT(authDTO.getLogin(), TT_MILLIS), HttpServletResponse.SC_OK);
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseDto(MSG_INCORRECT_DATA, HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

}
