package com.javaturtles.factory;

import com.javaturtles.exception.NoSuchTopicException;
import com.javaturtles.exception.ValidateException;
import com.javaturtles.util.MessageConverter;
import com.javaturtles.util.message_handler.MessageHandler;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.javaturtles.constants.FromUserMessageValidationConstants.NO_SUCH_TOPIC;

public class MessageHandlerFactory {
    private Map<String, MessageHandler> messageHandlerMap;

    public MessageHandlerFactory(Set<MessageHandler> messageHandlerMap) {
        this.messageHandlerMap = messageHandlerMap.stream().collect(Collectors.toMap(MessageHandler::getMessageRequest,
                messageHandler -> messageHandler));
    }

    public MessageHandler getUserMessageHandler(String messageHandler) throws NoSuchTopicException {
        MessageHandler handler = messageHandlerMap.get(messageHandler);
        if (handler == null) {
            throw new NoSuchTopicException(NO_SUCH_TOPIC);
        }
        return handler;
    }
}
