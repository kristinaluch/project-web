package com.javaturtles.dto;

import java.io.Serializable;
import java.util.Objects;

public class UserMessage implements Serializable {
    private String topic;
    private String payload;

    public UserMessage() {
    }

    public UserMessage(String topic, String payload) {
        this.topic = topic;
        this.payload = payload;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMessage that = (UserMessage) o;
        return Objects.equals(topic, that.topic) && Objects.equals(payload, that.payload);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, payload);
    }

    @Override
    public String toString() {
        return "FromUserMessage{" +
                "command='" + topic + '\'' +
                ", jsonMessage='" + payload + '\'' +
                '}';
    }
}
