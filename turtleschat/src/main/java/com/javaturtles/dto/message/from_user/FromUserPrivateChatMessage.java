package com.javaturtles.dto.message.from_user;

import java.util.Objects;

public class FromUserPrivateChatMessage {
    private final String userLogin;
    private final String receiverLogin;
    private final String message;

    public FromUserPrivateChatMessage(String userLogin, String receiverLogin, String message) {
        this.userLogin = userLogin;
        this.receiverLogin = receiverLogin;
        this.message = message;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getReceiverLogin() {
        return receiverLogin;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FromUserPrivateChatMessage that = (FromUserPrivateChatMessage) o;
        return Objects.equals(userLogin, that.userLogin) && Objects.equals(receiverLogin, that.receiverLogin) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userLogin, receiverLogin, message);
    }

    @Override
    public String toString() {
        return "FromUserPrivateChatMessage{" +
                "userLogin='" + userLogin + '\'' +
                ", interlocutorLogin='" + receiverLogin + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
