package com.javaturtles.dto.message.to_user;

import java.util.Date;
import java.util.Objects;

public class ToUserChatMessage {
    private final String userLogin;
    private final String receiverLogin;
    private final Date messageDate;
    private final String message;

    public ToUserChatMessage(String userLogin, String receiverLogin, Date messageDate, String message) {
        this.userLogin = userLogin;
        this.receiverLogin = receiverLogin;
        this.messageDate = messageDate;
        this.message = message;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getReceiverLogin() {
        return receiverLogin;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToUserChatMessage that = (ToUserChatMessage) o;
        return Objects.equals(userLogin, that.userLogin) && Objects.equals(receiverLogin, that.receiverLogin) && Objects.equals(messageDate, that.messageDate) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userLogin, receiverLogin, messageDate, message);
    }

    @Override
    public String toString() {
        return "ToUserPrivateChatMessage{" +
                "userLogin='" + userLogin + '\'' +
                ", interlocutorLogin='" + receiverLogin + '\'' +
                ", messageDate=" + messageDate +
                ", message='" + message + '\'' +
                '}';
    }
}
