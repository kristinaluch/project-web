package com.javaturtles.cache;

import org.eclipse.jetty.websocket.api.Session;

import java.util.Map;
import java.util.Set;

public class UserSessionCache {
    private Map<String, Session> userSessions;

    public UserSessionCache(Map<String, Session> userSessions) {
        this.userSessions = userSessions;
    }

    public void addSession(String login, Session session) {
        userSessions.put(login, session);
    }

    public Session getSession(String login) {
        return userSessions.get(login);
    }

    public void removeSession(String login) {
        userSessions.remove(login);
    }

    public Set<String> getLoginSet() {
        return userSessions.keySet();
    }

    public Map<String, Session> getUserSessions() {
        return userSessions;
    }
}
