package com.javaturtles.controller;

import com.javaturtles.dto.ResponseDto;
import com.javaturtles.service.AuthorizationService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public class AuthorizationController extends HttpServlet {

    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println(response);

        String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        System.out.println(body);
        ResponseDto responseDto;
        responseDto = authorizationService.authorizeUser(body);

        if (responseDto.getStatus() != HttpServletResponse.SC_OK) {
            response.getWriter().write(responseDto.getMessage());
            response.setStatus(responseDto.getStatus());
        } else {
            response.setHeader("access_token", responseDto.getMessage());
            response.setStatus(responseDto.getStatus());
        }
        System.out.println(responseDto);
    }
}
