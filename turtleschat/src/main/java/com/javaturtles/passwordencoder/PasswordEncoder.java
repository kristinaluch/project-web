package com.javaturtles.passwordencoder;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class PasswordEncoder {

    private String salt;

    public PasswordEncoder(String salt) {
        this.salt = salt;
    }

    public String getHash(String password) {
        return Hashing.sha256().hashString(password + salt, StandardCharsets.UTF_8).toString();
    }

}
