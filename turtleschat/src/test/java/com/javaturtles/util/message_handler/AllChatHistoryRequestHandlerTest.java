package com.javaturtles.util.message_handler;

import com.javaturtles.util.session_handler.ChatSessionsHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static com.javaturtles.constants.FromUserTopicConstants.ALL_CHAT_HISTORY_REQUEST;
import static com.javaturtles.testdata.AllChatHistoryRequestHandlerTestData.*;

public class AllChatHistoryRequestHandlerTest {
    private static final ChatSessionsHandler chatSessionsHandler = Mockito.mock(ChatSessionsHandler.class);

    private static final AllChatHistoryRequestHandler cut = new AllChatHistoryRequestHandler(chatSessionsHandler);

    @Test
    void processTest() {
        cut.process(CERTAIN_MESSAGE, CERTAIN_LOGIN);

        Mockito.verify(chatSessionsHandler, Mockito.times(SEND_ALL_CHAT_HISTORY_TIMES)).sendAllChatHistory(CERTAIN_LOGIN);
    }

    @Test
    void getMessageRequestTest() {
        String actual = cut.getMessageRequest();
        String expected = ALL_CHAT_HISTORY_REQUEST;
        Assertions.assertEquals(expected, actual);
    }
}
