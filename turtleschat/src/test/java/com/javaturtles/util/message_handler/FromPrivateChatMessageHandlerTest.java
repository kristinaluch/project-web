package com.javaturtles.util.message_handler;

import com.google.gson.Gson;

import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;

import com.javaturtles.util.session_handler.ChatSessionsHandler;
import com.javaturtles.util.validation.Validation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import static com.javaturtles.constants.FromUserTopicConstants.PRIVATE_CHAT_MESSAGE_FROM_USER;
import static com.javaturtles.constants.ResponseMessage.MSG_SUCCESS;
import static com.javaturtles.testdata.FromPrivateChatMessageHandlerTestData.*;

public class FromPrivateChatMessageHandlerTest {
    private static final ChatSessionsHandler chatSessionsHandler = Mockito.mock(ChatSessionsHandler.class);
    private static final Validation validation = Mockito.mock(Validation.class);
    private static final Gson gson = new Gson();

    private static final FromPrivateChatMessageHandler cut = new FromPrivateChatMessageHandler(chatSessionsHandler,
            validation, gson);

    static Arguments[] processTestArgs() {
        return new Arguments[] {
                Arguments.arguments(MESSAGE_1, CERTAIN_LOGIN_1, fromUserPrivateChatMessage1, MSG_SUCCESS, SEND_TIMES),
                Arguments.arguments(MESSAGE_2, CERTAIN_LOGIN_2, fromUserPrivateChatMessage2, MSG_SUCCESS, SEND_TIMES),
        };
    }

    @ParameterizedTest
    @MethodSource("processTestArgs")
    void processTest(String message, String login, FromUserPrivateChatMessage fromUserPrivateChatMessage,
                     String validationResult, int sendTimes) {
        Mockito.when(validation.getValidateResponse(fromUserPrivateChatMessage)).thenReturn(validationResult);

        cut.process(message, login);

        Mockito.verify(chatSessionsHandler, Mockito.times(sendTimes)).sendPrivateChatMessage(fromUserPrivateChatMessage);
    }

    @Test
    void getMessageRequestTest() {
        String actual = cut.getMessageRequest();
        String expected = PRIVATE_CHAT_MESSAGE_FROM_USER;
        Assertions.assertEquals(expected, actual);
    }
}
