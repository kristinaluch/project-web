package com.javaturtles.util;

import com.google.gson.Gson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import static com.javaturtles.testdata.MessageConverterTestData.*;

public class MessageConverterTest {
    private static final Gson gson = new Gson();

    private static final MessageConverter cut = new MessageConverter(gson);

    static Arguments[] createToUserMessageStringTestArgs() {
        return new Arguments[] {
          Arguments.arguments(USER_CONNECTION_ACTION, USER_LOGIN, USER_CONNECTION_EXPECTED),
          Arguments.arguments(USER_DISCONNECTION_ACTION, USER_LOGIN, USER_DISCONNECTION_EXPECTED),
        };
    }

    @ParameterizedTest
    @MethodSource("createToUserMessageStringTestArgs")
    void createToUserMessageStringTest(String action, String message, String expected) {
        String actual = cut.createToUserMessage(action, message);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] createToUserMessageObjectTestArgs() {
        return new Arguments[] {
                Arguments.arguments(USERS_LIST_ACTION, USERS_MAP, USERS_LIST_EXPECTED),
                Arguments.arguments(ALL_CHAT_MESSAGE_TO_USER_ACTION, ALL_CHAT_MESSAGE,
                        ALL_CHAT_MESSAGE_TO_USER_EXPECTED),
                Arguments.arguments(PRIVATE_CHAT_MESSAGE_TO_USER_ACTION, PRIVATE_CHAT_MESSAGE,
                        PRIVATE_CHAT_MESSAGE_TO_USER_EXPECTED),
                Arguments.arguments(ALL_CHAT_HISTORY_ACTION, CHAT_MESSAGES, ALL_CHAT_HISTORY_EXPECTED),
                Arguments.arguments(PRIVATE_CHAT_HISTORY_ACTION, PRIVATE_CHAT_HISTORY_MESSAGE,
                        PRIVATE_CHAT_HISTORY_EXPECTED),
        };
    }

    @ParameterizedTest
    @MethodSource("createToUserMessageObjectTestArgs")
    void createToUserMessageObjectTest(String action, Object message, String expected) {
        String actual = cut.createToUserMessage(action, message);
        Assertions.assertEquals(expected, actual);
    }
}
