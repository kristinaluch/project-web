package com.javaturtles.util.validation;

import com.javaturtles.dao.IDbService;
import com.javaturtles.dto.AuthorizationDto;
import com.javaturtles.dto.RegistrationDto;
import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;
import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;
import com.javaturtles.passwordencoder.PasswordEncoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import static com.javaturtles.constants.ResponseMessage.MSG_INVALID_EXCEEDED_CHARACTERS;
import static com.javaturtles.testdata.util.validation.ValidationTestData.*;

public class ValidationTest {

    private final PasswordEncoder passwordEncoder = Mockito.mock(PasswordEncoder.class);
    private final IDbService iDbService = Mockito.mock(IDbService.class);

    Validation cut = new Validation(iDbService, passwordEncoder);

    static Arguments[] getValidateResponseRegTestArgs(){
        return new Arguments[]{
                Arguments.arguments(CORRECT_REG_DTO_1, MSG_SUCCESS, CORRECT_REG_DTO_1.getLogin(), CORRECT_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(CORRECT_REG_DTO_2, MSG_SUCCESS, CORRECT_REG_DTO_2.getLogin(), CORRECT_REG_DTO_2.getEmail(), false, false),
                Arguments.arguments(CORRECT_REG_DTO_3, MSG_SUCCESS, CORRECT_REG_DTO_3.getLogin(), CORRECT_REG_DTO_3.getEmail(), false, false),
                Arguments.arguments(INCORRECT_LOGIN_REG_DTO_1, MSG_INVALID_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_1.getLogin(),INCORRECT_LOGIN_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(INCORRECT_LOGIN_REG_DTO_2, MSG_INVALID_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_2.getLogin(),INCORRECT_LOGIN_REG_DTO_2.getEmail(), false, false),
                Arguments.arguments(INCORRECT_LOGIN_REG_DTO_3, MSG_INVALID_INCORRECT_LOGIN, INCORRECT_LOGIN_REG_DTO_3.getLogin(),INCORRECT_LOGIN_REG_DTO_3.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_1, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_1.getLogin(), INCORRECT_PASSWORD_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_2, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_2.getLogin(), INCORRECT_PASSWORD_REG_DTO_2.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_3, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_3.getLogin(), INCORRECT_PASSWORD_REG_DTO_3.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_4, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_4.getLogin(), INCORRECT_PASSWORD_REG_DTO_4.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PASSWORD_REG_DTO_5, MSG_INVALID_INCORRECT_PASSWORD, INCORRECT_PASSWORD_REG_DTO_5.getLogin(), INCORRECT_PASSWORD_REG_DTO_5.getEmail(), false, false),
                Arguments.arguments(INCORRECT_CONFIRM_REG_DTO_1, MSG_INVALID_CONFIRM_PASSWORD_MISMATCH, INCORRECT_CONFIRM_REG_DTO_1.getLogin(), INCORRECT_CONFIRM_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_1, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_1.getLogin(), INCORRECT_EMAIL_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_2, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_2.getLogin(), INCORRECT_EMAIL_REG_DTO_2.getEmail(), false, false),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_3, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_3.getLogin(), INCORRECT_EMAIL_REG_DTO_3.getEmail(), false, false),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_4, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_4.getLogin(), INCORRECT_EMAIL_REG_DTO_4.getEmail(), false, false),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_5, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_5.getLogin(), INCORRECT_EMAIL_REG_DTO_5.getEmail(), false, false),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_6, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_6.getLogin(), INCORRECT_EMAIL_REG_DTO_6.getEmail(), false, false),
                Arguments.arguments(INCORRECT_EMAIL_REG_DTO_7, MSG_INVALID_INCORRECT_EMAIL, INCORRECT_EMAIL_REG_DTO_7.getLogin(), INCORRECT_EMAIL_REG_DTO_7.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_1, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_1.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_2, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_2.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_2.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_3, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_3.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_3.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_4, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_4.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_4.getEmail(), false, false),
                Arguments.arguments(INCORRECT_PHONE_NUMBER_REG_DTO_5, MSG_INVALID_INCORRECT_NUMBER, INCORRECT_PHONE_NUMBER_REG_DTO_5.getLogin(), INCORRECT_PHONE_NUMBER_REG_DTO_5.getEmail(), false, false),
                Arguments.arguments(INCORRECT_COMPANY_NAME_REG_DTO_1, MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS, INCORRECT_COMPANY_NAME_REG_DTO_1.getLogin(), INCORRECT_COMPANY_NAME_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(INCORRECT_NULL_REG_DTO_1, MSG_INVALID_EMPTY_NULL_FIELD, INCORRECT_NULL_REG_DTO_1.getLogin(), INCORRECT_NULL_REG_DTO_1.getEmail(), false, false),
                Arguments.arguments(null, MSG_INVALID_NULL, "", "", true, true),
                Arguments.arguments(INCORRECT_EMPTY_REG_DTO_1, MSG_INVALID_EMPTY_NULL_FIELD, "", "", false, false),
                Arguments.arguments(CORRECT_REG_DTO_1, MSG_INVALID_INCORRECT_LOGIN, CORRECT_REG_DTO_1.getLogin(), CORRECT_REG_DTO_1.getEmail(), true, false),
                Arguments.arguments(CORRECT_REG_DTO_2, MSG_INVALID_INCORRECT_EMAIL, CORRECT_REG_DTO_2.getLogin(), CORRECT_REG_DTO_2.getEmail(), false, true)
        };
    }

    @MethodSource("getValidateResponseRegTestArgs")
    @ParameterizedTest
    void getValidateResponseTest(RegistrationDto registrationDTO, String expected, String login, String email, boolean notUniqueLog, boolean notUniqueEmail) {

        Mockito.when(iDbService.isNotUniqueLogin(login)).thenReturn(notUniqueLog);
        Mockito.when(iDbService.isNotUniqueEmail(email)).thenReturn(notUniqueEmail);
        String actual = cut.getValidateResponse(registrationDTO);
        Assertions.assertEquals(expected, actual);

    }

    static Arguments[] getValidateResponseAuthTestArgs(){
        return new Arguments[]{

                Arguments.arguments(CORRECT_AUTH_DTO_1, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_SUCCESS),
                Arguments.arguments(CORRECT_AUTH_DTO_2, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_SUCCESS),
                Arguments.arguments(CORRECT_AUTH_DTO_3, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_SUCCESS),
                Arguments.arguments(CORRECT_AUTH_DTO_4, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_SUCCESS),
                Arguments.arguments(INCORRECT_LOGIN_AUTH_DTO_1, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_LOGIN_AUTH_DTO_2, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_LOGIN_AUTH_DTO_3, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_LOGIN_AUTH_DTO_4, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(INCORRECT_PASSWORD_AUTH_DTO_5, INCORRECT_HASH_PASSWORD_1, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_PASSWORD_AUTH_DTO_6, INCORRECT_HASH_PASSWORD_2, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_PASSWORD_AUTH_DTO_7, INCORRECT_HASH_PASSWORD_3, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_PASSWORD_AUTH_DTO_8, INCORRECT_HASH_PASSWORD_4, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_PASSWORD_AUTH_DTO_9, INCORRECT_HASH_PASSWORD_5, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_INCORRECT_PASSWORD),
                Arguments.arguments(INCORRECT_EMPTY_PASSWORD_AUTH_DTO_10, null, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_EMPTY_NULL_FIELD),
                Arguments.arguments(INCORRECT_EMPTY_LOGIN_AUTH_DTO_11, null, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_EMPTY_NULL_FIELD),
                Arguments.arguments(INCORRECT_NULL_LOGIN_AUTH_DTO_12, null, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_EMPTY_NULL_FIELD),
                Arguments.arguments(INCORRECT_NULL_PASSWORD_AUTH_DTO_13, null, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_EMPTY_NULL_FIELD),
                Arguments.arguments(CORRECT_AUTH_DTO_1, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_FALSE, CONFIRMED_USER_BY_EMAIL_TRUE, MSG_INVALID_CREDENTIALS),
                Arguments.arguments(CORRECT_AUTH_DTO_4, CORRECT_HASH_PASSWORD, CONFIRMED_USER_BY_LOGIN_TRUE, CONFIRMED_USER_BY_EMAIL_FALSE, MSG_INVALID_CREDENTIALS),
        };
    }

    @MethodSource("getValidateResponseAuthTestArgs")
    @ParameterizedTest
    void getValidateResponseTest(AuthorizationDto authDTO, String hashedPassword, boolean confirmedByLogin, boolean confirmedByEmail, String expected) {

        Mockito.when(passwordEncoder.getHash(authDTO.getPassword())).thenReturn(hashedPassword);
        Mockito.when(iDbService.confirmUserByLogin(authDTO.getLogin(), hashedPassword)).thenReturn(confirmedByLogin);
        Mockito.when(iDbService.confirmUserByEmail(authDTO.getLogin(), hashedPassword)).thenReturn(confirmedByEmail);
        String actual = cut.getValidateResponse(authDTO);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getValidateResponseAllChatMessageTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new FromUserAllChatMessage("loginUser", "Can you feel"),
                        MSG_SUCCESS),
                Arguments.arguments(new FromUserAllChatMessage("login", "Can you feel"),
                        MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(new FromUserAllChatMessage("loginUser", " It was part of the " +
                        "economy drive in preparation for Hate Week." +
                        " The flat was seven flights up, and Winston, who was thirty-nine and had a varicose ulcer " +
                        "above his right ankle, went slowly, resting several times on the way. On each landing, " +
                        "opposite the lift-shaft, the poster with the enormous face gazed from the wall. It was one of" +
                        " those pictures which are so contrived that the eyes follow you about when you move. " +
                        "BIG BROTHER IS WATCHING YOU, the caption beneath it ran."), MSG_INVALID_EXCEEDED_CHARACTERS),

         };
    }

    @MethodSource("getValidateResponseAllChatMessageTestArgs")
    @ParameterizedTest
    void getValidateResponseAllChatMessageTest(FromUserAllChatMessage fromUserAllChatMessage, String expected) {
        String actual = cut.getValidateResponse(fromUserAllChatMessage);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getValidateResponsePrivateChatMessageTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new FromUserPrivateChatMessage("firstUser", "secondUser",
                        "Hello word"), MSG_SUCCESS),
                Arguments.arguments(new FromUserPrivateChatMessage("first_User", "secondUser",
                        "Hello word"), MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(new FromUserPrivateChatMessage("firstUser", "secondUserrrrrrrr",
                        "Hello word"), MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments(new FromUserPrivateChatMessage("firstUser", "secondUser",
                        " It was part of the " +
                                "economy drive in preparation for Hate Week." +
                                " The flat was seven flights up, and Winston, who was thirty-nine and had a varicose " +
                                "ulcer above his right ankle, went slowly, resting several times on the way. On each " +
                                "landing, opposite the lift-shaft, the poster with the enormous face gazed from the " +
                                "wall. It was one of those pictures which are so contrived that the eyes follow you " +
                                "about when you move. BIG BROTHER IS WATCHING YOU, the caption beneath it ran."),
                        MSG_INVALID_EXCEEDED_CHARACTERS)

        };
    }

    @MethodSource("getValidateResponsePrivateChatMessageTestArgs")
    @ParameterizedTest
    void getValidateResponsePrivateChatMessageTest(FromUserPrivateChatMessage fromUserPrivateChatMessage, String expected) {
        String actual = cut.getValidateResponse(fromUserPrivateChatMessage);
        Assertions.assertEquals(expected, actual);

    }

    static Arguments[] getLoginValidateResponseTestArgs(){
        return new Arguments[]{
                Arguments.arguments("LoginUser", MSG_SUCCESS),
                Arguments.arguments("Login_user", MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments("Login", MSG_INVALID_INCORRECT_LOGIN),
                Arguments.arguments("Login@qwdh", MSG_INVALID_INCORRECT_LOGIN),
        };
    }

    @MethodSource("getLoginValidateResponseTestArgs")
    @ParameterizedTest
    void getLoginValidateResponseTest(String login, String expected) {
        String actual = cut.getLoginValidateResponse(login);
        Assertions.assertEquals(expected, actual);

    }

}
