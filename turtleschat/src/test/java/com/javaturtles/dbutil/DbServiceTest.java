package com.javaturtles.dbutil;

import com.javaturtles.constants.ConstantsForDB;
import com.javaturtles.dao.DbService;
import com.javaturtles.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.sql.*;

public class DbServiceTest {

    DbService cut = new DbService();
    private final Connection connection = Mockito.mock(Connection.class);
    private final Statement statement = Mockito.mock(Statement.class);
    private final PreparedStatement preparedStatement = Mockito.mock(PreparedStatement.class);
    private final ResultSet result = Mockito.mock(ResultSet.class);

    @Test
    void addUserTestTrue()
    {
        User user = new User("login", "password", "email", "phoneNumber",
                "company");
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            Mockito.when(connection.prepareStatement(ConstantsForDB.SQL_INSERT_USER)).thenReturn(preparedStatement);
            boolean actual = cut.addUser(user);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(1, user.getLogin());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, user.getEmail());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, user.getPassword());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(4, user.getPhoneNumber());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, user.getCompany());
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(6, null);
            Mockito.verify(preparedStatement, Mockito.times(1)).setString(7, null);
            Mockito.verify(preparedStatement, Mockito.times(1)).executeUpdate();
            Mockito.verify(preparedStatement, Mockito.times(1)).close();
            Assertions.assertEquals(true, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void addUserTestFalse()
    {
        User user = new User("login", "password", "email", "phoneNumber",
                "company");
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            Mockito.when(connection.prepareStatement(ConstantsForDB.SQL_INSERT_USER)).thenReturn(preparedStatement);
            Exception e = new SQLException();
            Mockito.doThrow(e).when(preparedStatement).setString(1, user.getLogin());
            boolean actual = cut.addUser(user);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void isUniqueLoginTrueTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String login = "login";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("login")).thenReturn(login);
            boolean actual = cut.isNotUniqueLogin(login);
            Assertions.assertEquals(true, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void isUniqueLoginFalseTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String login = "login";
            String loginFalse = "loginFalse";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("login")).thenReturn(login);
            boolean actual = cut.isNotUniqueLogin(loginFalse);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void isUniqueLoginFalseExeptionTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String login = "login";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Exception e = new SQLException();
            Mockito.doThrow(e).when(statement).executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);;
            boolean actual = cut.isNotUniqueLogin(login);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void isUniqueEmailTrueTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String email = "email";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("email")).thenReturn(email);
            boolean actual = cut.isNotUniqueEmail(email);
            Assertions.assertEquals(true, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void isUniqueEmailFalseTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String email = "login";
            String emailFalse = "emailFalse";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("email")).thenReturn(email);
            boolean actual = cut.isNotUniqueEmail(emailFalse);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void isUniqueEmailFalseExeptionTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String emailFalse = "emailFalse";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Exception e = new SQLException();
            Mockito.doThrow(e).when(statement).executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);;
            boolean actual = cut.isNotUniqueEmail(emailFalse);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmUserByLoginTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String login = "login";
            String password = "password";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("login")).thenReturn(login);
            Mockito.when(result.getString("password")).thenReturn(password);
            boolean actual = cut.confirmUserByLogin(login, password);
            Assertions.assertEquals(true, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmUserByLoginFalseTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String login = "login";
            String password = "password";
            String passwordFalse = "passwordFalse";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("login")).thenReturn(login);
            Mockito.when(result.getString("password")).thenReturn(password);
            boolean actual = cut.confirmUserByLogin(login, passwordFalse);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmUserByLoginFalseExeptionTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String login = "login";
            String password = "password";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Exception e = new SQLException();
            Mockito.doThrow(e).when(statement).executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);;
            boolean actual = cut.confirmUserByLogin(login, password);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmUserByEmailTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String email = "email";
            String password = "password";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("email")).thenReturn(email);
            Mockito.when(result.getString("password")).thenReturn(password);
            boolean actual = cut.confirmUserByEmail(email, password);
            Assertions.assertEquals(true, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmUserByEmailFalseTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String email = "email";
            String password = "password";
            String passwordFalse = "passwordFalse";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Mockito.when(statement.executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS)).thenReturn(result);
            Mockito.when(result.next()).thenReturn(true, false);
            Mockito.when(result.getString("email")).thenReturn(email);
            Mockito.when(result.getString("password")).thenReturn(password);
            boolean actual = cut.confirmUserByEmail(email, passwordFalse);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void confirmUserByEmailFalseExeptionTest()
    {
        try (MockedStatic<DriverManager> mockDriveManager = Mockito.mockStatic(DriverManager.class)) {
            PropertyFileService pr = new PropertyFileService();
            mockDriveManager.when(() -> DriverManager.getConnection(pr.getDBHostFromProperty(), pr.getDBLoginFromProperty(),
                    pr.getDBPasswordFromProperty())).thenReturn(connection);
            String email = "email";
            String password = "password";
            Mockito.when(connection.createStatement()).thenReturn(statement);
            Exception e = new SQLException();
            Mockito.doThrow(e).when(statement).executeQuery(ConstantsForDB.SQL_SELECT_ALL_USERS);;
            boolean actual = cut.confirmUserByEmail(email, password);
            Assertions.assertEquals(false, actual);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
