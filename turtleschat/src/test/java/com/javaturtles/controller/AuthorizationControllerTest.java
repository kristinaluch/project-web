package com.javaturtles.controller;

import com.javaturtles.dto.ResponseDto;
import com.javaturtles.service.AuthorizationService;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static com.javaturtles.testdata.controller.AuthorizationControllerTestData.*;

class AuthorizationControllerTest {

    AuthorizationService authorizationService = Mockito.mock(AuthorizationService.class);
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
    BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
    Stream<String> stream = Mockito.mock(Stream.class);
    Collector collector = Mockito.mock(Collector.class);
    PrintWriter printWriter = Mockito.mock(PrintWriter.class);

    AuthorizationController cut = new AuthorizationController(authorizationService);

    static Arguments[] doPostTestArgs() {
        return new Arguments[] {
                Arguments.arguments(BODY_1, RESPONSE_DTO_CORRECT_1, 1, 1, 0),
                Arguments.arguments(BODY_2, RESPONSE_DTO_INCORRECT_2, 0, 1, 1),
                Arguments.arguments(BODY_3, RESPONSE_DTO_INCORRECT_3, 0, 1, 1),
                Arguments.arguments(null, RESPONSE_DTO_INCORRECT_NULL, 0, 1, 1)
        };
    }

    @ParameterizedTest
    @MethodSource("doPostTestArgs")
    void doPostTest(String body, ResponseDto responseDto, int setHeaderTimes, int setStatusTimes,
                    int writeErrorMessageTimes) throws ServletException, IOException {

        try (MockedStatic<Collectors> mockedCollectors = Mockito.mockStatic(Collectors.class)) {
            mockedCollectors.when(() -> Collectors.joining(System.lineSeparator())).thenReturn(collector);
            Mockito.when(request.getReader()).thenReturn(bufferedReader);
            Mockito.when(bufferedReader.lines()).thenReturn(stream);
            Mockito.when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(body);
            Mockito.when(authorizationService.authorizeUser(body)).thenReturn(responseDto);
            Mockito.when(response.getWriter()).thenReturn(printWriter);

            cut.doPost(request, response);

            Mockito.verify(printWriter, Mockito.times(writeErrorMessageTimes)).write(responseDto.getMessage());
            Mockito.verify(response, Mockito.times(setHeaderTimes)).setHeader("access_token", responseDto.getMessage());
            Mockito.verify(response, Mockito.times(setStatusTimes)).setStatus(responseDto.getStatus());
        }
    }

}