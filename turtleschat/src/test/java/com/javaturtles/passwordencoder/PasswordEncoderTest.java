package com.javaturtles.passwordencoder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static com.javaturtles.testdata.passwordencoder.PasswordEncoderTestData.*;

public class PasswordEncoderTest {

    private static final String SALT = "FjSO&@MPH3D21GUdq99wHv24po_OLpt88nzitWLOUGY@PYPH";

    PasswordEncoder cut = new PasswordEncoder(SALT);

    static Arguments[] getHashTestArgs(){
        return new Arguments[]{
                Arguments.arguments(PASSWORD_1, HASHED_PASSWORD_1),
                Arguments.arguments(PASSWORD_2, HASHED_PASSWORD_2),
        };
    }

    @MethodSource("getHashTestArgs")
    @ParameterizedTest
    void hashPasswordTest(String password, String expected) {

        String actual = cut.getHash(password);
        Assertions.assertEquals(expected, actual);

    }
}
