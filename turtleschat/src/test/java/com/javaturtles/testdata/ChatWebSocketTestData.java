package com.javaturtles.testdata;

import com.google.gson.Gson;
import com.javaturtles.dto.UserMessage;
import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;
import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;
import java.net.HttpCookie;
import java.util.LinkedList;
import java.util.List;
import static com.javaturtles.constants.FromUserTopicConstants.*;

public class ChatWebSocketTestData {
    public static final String LOGIN = "login";
    public static final String CERTAIN_LOGIN = "certainLogin";
    public static final String RECEIVER_LOGIN = "receiverLogin";
    public static final String COOKIE_KEY = "cookie_key";
    public static final String COOKIE_VALUE = "cookie_value";
    public static final String CERTAIN_MESSAGE = "Certain message from user";
    public static final int NOTIFY_DISCONNECTED_TIMES = 1;
    public static final int REMOVE_SESSION_TIMES = 1;
    public static final int REGISTER_SOCKET_TIMES = 1;
    public static final int NOTIFY_CONNECTED_TIMES = 1;
    public static final int SEND_USERS_LIST_TIMES = 1;
    public static final int SEND_ALL_CHAT_HISTORY_TIMES = 1;
    public static final HttpCookie HTTP_COOKIE_1 = new HttpCookie(COOKIE_KEY, COOKIE_VALUE);
    public static final HttpCookie HTTP_COOKIE_2 = new HttpCookie(LOGIN, CERTAIN_LOGIN);
    public static final List<HttpCookie> HTTP_COOKIE_LIST = new LinkedList<>(List.of(HTTP_COOKIE_1, HTTP_COOKIE_2));
    public static final String ERROR_REASON = "server error";
    public static final int PROCESS_TIMES = 1;
    public static final String TOPIC_1 = ALL_CHAT_HISTORY_REQUEST;
    public static final String TOPIC_2 = PRIVATE_CHAT_HISTORY_REQUEST;
    public static final String TOPIC_3 = ALL_CHAT_MESSAGE_FROM_USER;
    public static final String TOPIC_4 = PRIVATE_CHAT_MESSAGE_FROM_USER;
    public static final String PAYLOAD_1 = "";
    public static final String PAYLOAD_2 = RECEIVER_LOGIN;
    public static final String PAYLOAD_3 = "{\"userLogin\":\"certainLogin\",\"message\":\"Certain message from user\"}";
    public static final String PAYLOAD_4 = "{\"userLogin\":\"certainLogin\",\"receiverLogin\":\"receiverLogin\"," +
            "\"message\":\"Certain message from user\"}";
    public static final String MESSAGE_1 = "{\"topic\":\"all_chat_history_request\",\"payload\":\"\"}";
    public static final String MESSAGE_2 = "{\"topic\":\"private_chat_history_request\",\"payload\":\"receiverLogin\"}";
    public static final String MESSAGE_3 = "{\"topic\":\"all_chat_message\",\"payload\":\"{\\\"userLogin\\\":" +
            "\\\"certainLogin\\\",\\\"message\\\":\\\"Certain message from user\\\"}\"}";
    public static final String MESSAGE_4 = "{\"topic\":\"private_chat_message\",\"payload\":\"{\\\"userLogin\\\":" +
            "\\\"certainLogin\\\",\\\"receiverLogin\\\":\\\"receiverLogin\\\",\\\"message\\\":" +
            "\\\"Certain message from user\\\"}\"}";
}
