package com.javaturtles.testdata;

import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;
import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;
import com.javaturtles.dto.message.to_user.PrivateChatHistoryMessage;
import com.javaturtles.dto.message.to_user.ToUserChatMessage;
import com.javaturtles.entity.ChatMessage;

import java.util.*;

import static com.javaturtles.constants.UserStatusConstants.OFFLINE;
import static com.javaturtles.constants.UserStatusConstants.ONLINE;

public class ChatSessionsHandlerTestData {
    public static final String CERTAIN_LOGIN = "certainLogin";
    public static final String CERTAIN_LOGIN_1 = "certainLogin1";
    public static final String CERTAIN_LOGIN_2 = "certainLogin2";
    public static final String CERTAIN_LOGIN_3 = "certainLogin3";
    public static final String RECEIVER_LOGIN = "receiverLogin";
    public static final String RECEIVER_LOGIN_1 = "receiverLogin1";
    public static final String RECEIVER_LOGIN_2 = "receiverLogin2";
    public static final List<String> USER_LIST = new ArrayList<>(List.of(CERTAIN_LOGIN_1, CERTAIN_LOGIN_2,
            CERTAIN_LOGIN_3));
    public static final Set<String> ONLINE_USERS = new HashSet<>(List.of(CERTAIN_LOGIN_1, CERTAIN_LOGIN_3));
    public static final Map<String, String> USERS_MAP = new HashMap<>();
    public static final String TO_USER_USERS_LIST_MESSAGE = "{\"topic\":\"users_list\",\"payload\":" +
            "\"{\\\"certainLogin3\\\":\\\"online\\\",\\\"certainLogin1\\\":\\\"online\\\",\\\"certainLogin2\\\":" +
            "\\\"offline\\\"}\"}";
    public static final int SEND_LIST_TIMES = 1;
    public static final long NOW_MILLIS = 2_000_000L;
    public static final long CHAT_MILLIS_1 = 1_900_000L;
    public static final long CHAT_MILLIS_2 = 1_950_000L;
    public static final Date NOW_DATE = new Date(NOW_MILLIS);
    public static final Date CHAT_DATE_1 = new Date(CHAT_MILLIS_1);
    public static final Date CHAT_DATE_2 = new Date(CHAT_MILLIS_2);
    public static final String CERTAIN_ALL_CHAT_MESSAGE = "Certain all-chat message from certain user";
    public static final String CERTAIN_PRIVATE_CHAT_MESSAGE = "Certain private-chat message from certain user";
    public static final FromUserAllChatMessage FROM_USER_ALL_CHAT_MESSAGE = new FromUserAllChatMessage(CERTAIN_LOGIN,
            CERTAIN_ALL_CHAT_MESSAGE);
    public static final String CHAT_MESSAGE_STRING_1 = "Message from user";
    public static final String CHAT_MESSAGE_STRING_2 = "Message from other user";
    public static final FromUserPrivateChatMessage FROM_USER_PRIVATE_CHAT_MESSAGE =
            new FromUserPrivateChatMessage(CERTAIN_LOGIN, RECEIVER_LOGIN, CERTAIN_PRIVATE_CHAT_MESSAGE);
    public static final ToUserChatMessage ALL_CHAT_MESSAGE_OBJECT = new ToUserChatMessage(CERTAIN_LOGIN, null,
            NOW_DATE, CERTAIN_ALL_CHAT_MESSAGE);
    public static final ToUserChatMessage PRIVATE_CHAT_MESSAGE_OBJECT = new ToUserChatMessage(CERTAIN_LOGIN,
            RECEIVER_LOGIN, NOW_DATE, CERTAIN_PRIVATE_CHAT_MESSAGE);
    public static final String TO_USER_ALL_CHAT_MESSAGE = "{\"topic\":\"all_chat_message\",\"payload\":" +
            "\"{\\\"userLogin\\\":\\\"certainLogin\\\",\\\"messageDate\\\":\\\"Jan 1, 1970, 2:33:20 AM\\\"," +
            "\\\"message\\\":\\\"Certain all-chat message from certain user\\\"}\"}";
    public static final String TO_USER_PRIVATE_CHAT_MESSAGE = "{\"topic\":\"private_chat_message\"," +
            "\"payload\":\"{\\\"userLogin\\\":\\\"certainLogin\\\",\\\"receiverLogin\\\":\\\"receiverLogin\\\"," +
            "\\\"messageDate\\\":\\\"Jan 1, 1970, 2:33:20 AM\\\",\\\"message\\\":" +
            "\\\"Certain private-chat message from certain user\\\"}\"}";
    public static final ChatMessage CHAT_MESSAGE_1 = new ChatMessage(CERTAIN_LOGIN, CHAT_DATE_1, CHAT_MESSAGE_STRING_1);
    public static final ChatMessage CHAT_MESSAGE_2 = new ChatMessage(RECEIVER_LOGIN, CHAT_DATE_2,
            CHAT_MESSAGE_STRING_2);
    public static final List<ChatMessage> CHAT_MESSAGE_LIST = new ArrayList<>(List.of(CHAT_MESSAGE_1, CHAT_MESSAGE_2));
    public static final PrivateChatHistoryMessage PRIVATE_CHAT_HISTORY_MESSAGE = new PrivateChatHistoryMessage(CERTAIN_LOGIN, RECEIVER_LOGIN,
            CHAT_MESSAGE_LIST);
    public static final String TO_USER_ALL_CHAT_HISTORY = "{\"topic\":\"all_chat_history\",\"payload\":" +
            "\"[{\\\"login\\\":\\\"certainLogin\\\",\\\"date\\\":\\\"Jan 1, 1970, 2:31:40 AM\\\"," +
            "\\\"message\\\":\\\"Message from user\\\"},{\\\"login\\\":\\\"receiverLogin\\\"," +
            "\\\"date\\\":\\\"Jan 1, 1970, 2:32:30 AM\\\",\\\"message\\\":\\\"Message from other user\\\"}]\"}";
    public static final String TO_USER_PRIVATE_CHAT_HISTORY = "{\"topic\":\"private_chat_history\"," +
            "\"payload\":\"{\\\"user\\\":\\\"certainLogin\\\",\\\"receiver\\\":\\\"receiverLogin\\\"," +
            "\\\"chatMessages\\\":[{\\\"login\\\":\\\"certainLogin\\\",\\\"date\\\":\\\"Jan 1, 1970, 2:31:40 AM\\\"," +
            "\\\"message\\\":\\\"Message from user\\\"},{\\\"login\\\":\\\"receiverLogin\\\"," +
            "\\\"date\\\":\\\"Jan 1, 1970, 2:32:30 AM\\\",\\\"message\\\":\\\"Message from other user\\\"}]}\"}";
    public static final String TO_USER_CONNECTION = "{\"topic\":\"user_connection\",\"payload\":\"certainLogin\"}";
    public static final String TO_USER_DISCONNECTION = "{\"topic\":\"user_disconnection\"," +
            "\"payload\":\"certainLogin\"}";

    static {
        USERS_MAP.put(CERTAIN_LOGIN_1, ONLINE);
        USERS_MAP.put(CERTAIN_LOGIN_2, OFFLINE);
        USERS_MAP.put(CERTAIN_LOGIN_3, ONLINE);
    }
}
