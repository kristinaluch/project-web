package com.javaturtles.testdata.service;


import com.javaturtles.dto.RegistrationDto;
import com.javaturtles.dto.ResponseDto;
import com.javaturtles.model.User;

public class RegistrationServiceTestData {

    public static final String MSG_SUCCESS = "OK";
    public static final String MSG_INVALID_INCORRECT_LOGIN = "Incorrect login";
    public static final String MSG_CREATED = "CREATED";
    public static final String MSG_INCORRECT_DATA = "Incorrect data";

    private static final String CORRECT_LOGIN_1 = "userLogin";
    private static final String INCORRECT_LOGIN_1 = "user_login";
    private static final String CORRECT_PASSWORD_1 = "passworD1";
    private static final String CORRECT_CONFIRM_CORRECT_PASSWORD_1 = "passworD1";
    private static final String CORRECT_EMAIL_1 = "email@qwe.com";
    private static final String CORRECT_PHONE_NUMBER = "+380669801221";
    private static final String CORRECT_COMPANY_NAME_1 = "";

    public static String CORRECT_BODY_1 = "{\n" +
            "    \"login\":" + CORRECT_LOGIN_1 + ",\n" +
            "    \"password\":" + CORRECT_PASSWORD_1 + ",\n" +
            "    \"confirmPassword\":" + CORRECT_CONFIRM_CORRECT_PASSWORD_1 + ",\n" +
            "    \"email\":" + CORRECT_EMAIL_1 + ",\n" +
            "    \"phoneNumber\":" + CORRECT_PHONE_NUMBER + ",\n" +
            "    \"company\":" + CORRECT_COMPANY_NAME_1 + "\n" +
            "}";


    public static final RegistrationDto CORRECT_REG_DTO_1 =
            new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD_1,
                    CORRECT_CONFIRM_CORRECT_PASSWORD_1, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);

    public static final String HASHED_PASSWORD = "HASHED_PASSWORD";

    public static final User USER_1 = new User(CORRECT_LOGIN_1, HASHED_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);

    public static final ResponseDto RESPONSE_OBJECT_CORRECT = new ResponseDto(MSG_CREATED, 201);

    public static final RegistrationDto INCORRECT_LOGIN_REG_DTO_1 = new RegistrationDto(INCORRECT_LOGIN_1, CORRECT_PASSWORD_1, CORRECT_CONFIRM_CORRECT_PASSWORD_1, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);

    public static final String BODY_INCORRECT_LOGIN = "{\n" +
            "    \"login\":user_login,\n" +
            "    \"password\":passworD1,\n" +
            "    \"confirmPassword\":passworD1,\n" +
            "    \"email\":email@qwe.com,\n" +
            "    \"phoneNumber\":+380669801221,\n" +
            "    \"company\":\n" +
            "}";

    public static final ResponseDto RESPONSE_OBJECT_INCORRECT = new ResponseDto(MSG_INVALID_INCORRECT_LOGIN, 400);

    public static final String BODY_EXCEPTION = "{\n" +
            "    \"login\":user_login,\n" +
            "    \"password\":passworD1,\n" +
            "    \"something\"" + "\"anything\"" +
            "    \"company\":\n" +
            "}";

    public static final ResponseDto RESPONSE_OBJECT_EXCEPTION = new ResponseDto(MSG_INCORRECT_DATA, 400);

}
