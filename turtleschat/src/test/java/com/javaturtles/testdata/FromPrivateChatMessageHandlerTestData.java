package com.javaturtles.testdata;

import com.javaturtles.dto.message.from_user.FromUserAllChatMessage;
import com.javaturtles.dto.message.from_user.FromUserPrivateChatMessage;

public class FromPrivateChatMessageHandlerTestData {
    public static final String MESSAGE_1 = "{\"userLogin\":\"certainLogin1\",\"receiverLogin\":\"receiverLogin1\"," +
            "\"message\":\"Certain message from user 1\"}";
    public static final String MESSAGE_2 = "{\"userLogin\":\"certainLogin2\",\"receiverLogin\":\"receiverLogin2\"," +
            "\"message\":\"Certain message from user 2\"}";
    public static final String CERTAIN_LOGIN_1 = "certainLogin1";
    public static final String CERTAIN_LOGIN_2 = "certainLogin2";
    public static final String RECEIVER_LOGIN_1 = "receiverLogin1";
    public static final String RECEIVER_LOGIN_2 = "receiverLogin2";
    public static final String CERTAIN_MESSAGE_1 = "Certain message from user 1";
    public static final String CERTAIN_MESSAGE_2 = "Certain message from user 2";
    public static final FromUserPrivateChatMessage fromUserPrivateChatMessage1 =
            new FromUserPrivateChatMessage(CERTAIN_LOGIN_1, RECEIVER_LOGIN_1, CERTAIN_MESSAGE_1);
    public static final FromUserPrivateChatMessage fromUserPrivateChatMessage2 =
            new FromUserPrivateChatMessage(CERTAIN_LOGIN_2, RECEIVER_LOGIN_2, CERTAIN_MESSAGE_2);
    public static final int SEND_TIMES = 1;
}
