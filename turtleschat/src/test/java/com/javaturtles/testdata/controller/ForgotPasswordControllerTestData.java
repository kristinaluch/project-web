package com.javaturtles.testdata.controller;

public class ForgotPasswordControllerTestData {

    public static final String MSG_CANNOT_SEND = "Cannot send message";
    public static final String MSG_NOT_POSSIBLE_RECOVER = "Recover not possible";
    public static final String MSG_SUCCESS_PASSWORD_RECOVER = "The password has been changed";
}
