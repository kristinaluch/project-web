package com.javaturtles.testdata.controller;

import com.javaturtles.dto.ResponseDto;

import javax.servlet.http.HttpServletResponse;

public class AuthorizationControllerTestData {

    public static final String JWT_TOKEN = "QWERTYASDFGHJKL6543FHKNV515LLL";
    public static final String MSG_INVALID_INCORRECT_LOGIN = "Incorrect login";
    public static final String MSG_INCORRECT_DATA = "Incorrect data";
    public static final String MSG_INVALID_NULL = "Fields cannot be null";
    public static final String BODY_1 = "{\"login\":\"log\",\"password\":\"pass\"}";
    public static final String BODY_2 = "{\"login\":\"bad log\",\"password\":\"bad pass\"}";
    public static final String BODY_3 = "not serializable string";
    public static final ResponseDto RESPONSE_DTO_CORRECT_1 = new ResponseDto(JWT_TOKEN, HttpServletResponse.SC_OK);
    public static final ResponseDto RESPONSE_DTO_INCORRECT_2 = new ResponseDto(MSG_INVALID_INCORRECT_LOGIN, HttpServletResponse.SC_UNAUTHORIZED);
    public static final ResponseDto RESPONSE_DTO_INCORRECT_3 = new ResponseDto(MSG_INCORRECT_DATA, HttpServletResponse.SC_UNAUTHORIZED);
    public static final ResponseDto RESPONSE_DTO_INCORRECT_NULL = new ResponseDto(MSG_INVALID_NULL, HttpServletResponse.SC_UNAUTHORIZED);


}
