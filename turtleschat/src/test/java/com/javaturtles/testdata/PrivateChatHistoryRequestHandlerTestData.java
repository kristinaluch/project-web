package com.javaturtles.testdata;

public class PrivateChatHistoryRequestHandlerTestData {
    public static final String CERTAIN_LOGIN = "certain_login";
    public static final String CERTAIN_MESSAGE = "certain message";
    public static final int SEND_HISTORY_TIMES = 1;
}
