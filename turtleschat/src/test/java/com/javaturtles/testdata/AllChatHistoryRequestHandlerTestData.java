package com.javaturtles.testdata;

public class AllChatHistoryRequestHandlerTestData {
    public static final String CERTAIN_MESSAGE = "certain message";
    public static final String CERTAIN_LOGIN = "certain login";
    public static final int SEND_ALL_CHAT_HISTORY_TIMES = 1;
}
