package com.javaturtles.testdata.util.validation;

import com.javaturtles.dto.AuthorizationDto;
import com.javaturtles.dto.RegistrationDto;

public class ValidationTestData {

    private static final String CORRECT_LOGIN_1 = "userLogin";
    private static final String CORRECT_LOGIN_2 = "user34Login";
    private static final String CORRECT_LOGIN_3 = "12345678";
    private static final String CORRECT_AUTH_LOGIN_4 = "useremail@gmail.com";
    private static final String INCORRECT_LOGIN_1 = "$user#login";
    private static final String INCORRECT_LOGIN_2 = "user";
    private static final String INCORRECT_LOGIN_3 = "TooLongUserLoginForAuth";
    private static final String INCORRECT_LOGIN_AUTH_4 = "useremail@gmail";
    private static final String CORRECT_PASSWORD = "Password007";
    private static final String INCORRECT_PASSWORD_1 = "UserPassword";
    private static final String INCORRECT_PASSWORD_2 = "password007";
    private static final String INCORRECT_PASSWORD_3 = "Pas12";
    private static final String INCORRECT_PASSWORD_4 = "Password123456789TooLong";
    private static final String INCORRECT_PASSWORD_5 = "PASSWORD1";
    public static final String CORRECT_HASH_PASSWORD = "9cfb2b19b64a380621758cfb527d2204c4aa618e0419009f39ef78577b438a60";
    public static final String INCORRECT_HASH_PASSWORD_1 = "9acfc1264aa1dfbd1a8a18398cfa83cbb0191ccc241c536e74b1e17370087543";
    public static final String INCORRECT_HASH_PASSWORD_2 = "0cde11cc0b7b86a42b1fce812a074c76c93d8e9b23781217fd14a270166593b0";
    public static final String INCORRECT_HASH_PASSWORD_3 = "b3d02c8c2be516b6f730322dfdb1c78b0f781561cf464b05388cf2fe6dde670c";
    public static final String INCORRECT_HASH_PASSWORD_4 = "be23eb0b83cae6f424ddd7d033c5d3de0e227ed5ba1b9d6667e022f054916b81";
    public static final String INCORRECT_HASH_PASSWORD_5 = "3e64f92f0f82b07d184c543c546808b1f9dd6228471c9bfb453725be2e8d46a7";
    private static final String CORRECT_CONFIRM_CORRECT_PASSWORD = "Password007";
    private static final String INCORRECT_CONFIRM_CORRECT_PASSWORD = "passworD007";
    private static final String CORRECT_CONFIRM_INCORRECT_PASSWORD_1 = "UserPassword";
    private static final String CORRECT_CONFIRM_INCORRECT_PASSWORD_2 = "password007";
    private static final String CORRECT_CONFIRM_INCORRECT_PASSWORD_3 = "Pas12";
    private static final String CORRECT_CONFIRM_INCORRECT_PASSWORD_4 = "Password123456789TooLong";
    private static final String CORRECT_CONFIRM_INCORRECT_PASSWORD_5 = "PASSWORD1";
    private static final String CORRECT_EMAIL_1 = "email@qwe.com";
    private static final String CORRECT_EMAIL_2 = "123456@qwe.com";
    private static final String INCORRECT_EMAIL_1 = "email.qwe.com";
    private static final String INCORRECT_EMAIL_2 = "email@qwecom";
    private static final String INCORRECT_EMAIL_3 = "email@.com";
    private static final String INCORRECT_EMAIL_4 = "email@qwe.q";
    private static final String INCORRECT_EMAIL_5 = "email@qwe.qwertyu";
    private static final String INCORRECT_EMAIL_6 = "email@123456789012345678901234567890123.com";
    private static final String INCORRECT_EMAIL_7 = "123456789012345678901234567890123@qwe.com"; //33 sym
    private static final String CORRECT_PHONE_NUMBER = "+380669801221";
    private static final String INCORRECT_PHONE_NUMBER_1 = "380669801221";
    private static final String INCORRECT_PHONE_NUMBER_2 = "+380669";
    private static final String INCORRECT_PHONE_NUMBER_3 = "+1234567890123456";
    private static final String INCORRECT_PHONE_NUMBER_4 = "+1234567";
    private static final String INCORRECT_PHONE_NUMBER_5 = "+38066801BMW";
    private static final String CORRECT_COMPANY_NAME_1 = "";
    private static final String CORRECT_COMPANY_NAME_2 = "Qwerty";
    private static final String INCORRECT_COMPANY_NAME_1 = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901";
    public static final RegistrationDto CORRECT_REG_DTO_1 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto CORRECT_REG_DTO_2 = new RegistrationDto(CORRECT_LOGIN_2, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_2, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_2);
    public static final RegistrationDto CORRECT_REG_DTO_3 = new RegistrationDto(CORRECT_LOGIN_3, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_2, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_2);
    public static final RegistrationDto INCORRECT_LOGIN_REG_DTO_1 = new RegistrationDto(INCORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_LOGIN_REG_DTO_2 = new RegistrationDto(INCORRECT_LOGIN_2, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_LOGIN_REG_DTO_3 = new RegistrationDto(INCORRECT_LOGIN_3, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PASSWORD_REG_DTO_1 = new RegistrationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_1, CORRECT_CONFIRM_INCORRECT_PASSWORD_1, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PASSWORD_REG_DTO_2 = new RegistrationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_2, CORRECT_CONFIRM_INCORRECT_PASSWORD_2, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PASSWORD_REG_DTO_3 = new RegistrationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_3, CORRECT_CONFIRM_INCORRECT_PASSWORD_3, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PASSWORD_REG_DTO_4 = new RegistrationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_4, CORRECT_CONFIRM_INCORRECT_PASSWORD_4, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PASSWORD_REG_DTO_5 = new RegistrationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_5, CORRECT_CONFIRM_INCORRECT_PASSWORD_5, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_CONFIRM_REG_DTO_1 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, INCORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMAIL_REG_DTO_1 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, INCORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMAIL_REG_DTO_2 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, INCORRECT_EMAIL_2, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMAIL_REG_DTO_3 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, INCORRECT_EMAIL_3, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMAIL_REG_DTO_4 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, INCORRECT_EMAIL_4, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMAIL_REG_DTO_5 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, INCORRECT_EMAIL_5, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMAIL_REG_DTO_6 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, INCORRECT_EMAIL_6, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMAIL_REG_DTO_7 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, INCORRECT_EMAIL_7, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PHONE_NUMBER_REG_DTO_1 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, INCORRECT_PHONE_NUMBER_1, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PHONE_NUMBER_REG_DTO_2 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, INCORRECT_PHONE_NUMBER_2, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PHONE_NUMBER_REG_DTO_3 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, INCORRECT_PHONE_NUMBER_3, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PHONE_NUMBER_REG_DTO_4 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, INCORRECT_PHONE_NUMBER_4, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_PHONE_NUMBER_REG_DTO_5 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, INCORRECT_PHONE_NUMBER_5, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_COMPANY_NAME_REG_DTO_1 = new RegistrationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, INCORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_NULL_REG_DTO_1 = new RegistrationDto(null, CORRECT_PASSWORD, CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_1, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_1);
    public static final RegistrationDto INCORRECT_EMPTY_REG_DTO_1 = new RegistrationDto(CORRECT_LOGIN_2, "", CORRECT_CONFIRM_CORRECT_PASSWORD, CORRECT_EMAIL_2, CORRECT_PHONE_NUMBER, CORRECT_COMPANY_NAME_2);
    public static final AuthorizationDto CORRECT_AUTH_DTO_1 = new AuthorizationDto(CORRECT_LOGIN_1, CORRECT_PASSWORD);
    public static final AuthorizationDto CORRECT_AUTH_DTO_2 = new AuthorizationDto(CORRECT_LOGIN_2, CORRECT_PASSWORD);
    public static final AuthorizationDto CORRECT_AUTH_DTO_3 = new AuthorizationDto(CORRECT_LOGIN_3, CORRECT_PASSWORD);
    public static final AuthorizationDto CORRECT_AUTH_DTO_4 = new AuthorizationDto(CORRECT_AUTH_LOGIN_4, CORRECT_PASSWORD);
    public static final AuthorizationDto INCORRECT_LOGIN_AUTH_DTO_1 = new AuthorizationDto(INCORRECT_LOGIN_1, CORRECT_PASSWORD);
    public static final AuthorizationDto INCORRECT_LOGIN_AUTH_DTO_2 = new AuthorizationDto(INCORRECT_LOGIN_2, CORRECT_PASSWORD);
    public static final AuthorizationDto INCORRECT_LOGIN_AUTH_DTO_3 = new AuthorizationDto(INCORRECT_LOGIN_3, CORRECT_PASSWORD);
    public static final AuthorizationDto INCORRECT_LOGIN_AUTH_DTO_4 = new AuthorizationDto(INCORRECT_LOGIN_AUTH_4, CORRECT_PASSWORD);
    public static final AuthorizationDto INCORRECT_PASSWORD_AUTH_DTO_5 = new AuthorizationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_1);
    public static final AuthorizationDto INCORRECT_PASSWORD_AUTH_DTO_6 = new AuthorizationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_2);
    public static final AuthorizationDto INCORRECT_PASSWORD_AUTH_DTO_7 = new AuthorizationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_3);
    public static final AuthorizationDto INCORRECT_PASSWORD_AUTH_DTO_8 = new AuthorizationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_4);
    public static final AuthorizationDto INCORRECT_PASSWORD_AUTH_DTO_9 = new AuthorizationDto(CORRECT_LOGIN_1, INCORRECT_PASSWORD_5);
    public static final AuthorizationDto INCORRECT_EMPTY_PASSWORD_AUTH_DTO_10 = new AuthorizationDto(CORRECT_LOGIN_1, " ");
    public static final AuthorizationDto INCORRECT_EMPTY_LOGIN_AUTH_DTO_11 = new AuthorizationDto(" ", CORRECT_PASSWORD);
    public static final AuthorizationDto INCORRECT_NULL_LOGIN_AUTH_DTO_12 = new AuthorizationDto(null, CORRECT_PASSWORD);
    public static final AuthorizationDto INCORRECT_NULL_PASSWORD_AUTH_DTO_13 = new AuthorizationDto(CORRECT_LOGIN_1, null);
    public static final boolean CONFIRMED_USER_BY_EMAIL_TRUE = true;
    public static final boolean CONFIRMED_USER_BY_EMAIL_FALSE = false;
    public static final boolean CONFIRMED_USER_BY_LOGIN_TRUE = true;
    public static final boolean CONFIRMED_USER_BY_LOGIN_FALSE = false;
    public static final String MSG_SUCCESS = "OK";
    public static final String MSG_INVALID_EMPTY_NULL_FIELD = "Fields cannot be null or empty";
    public static final String MSG_INVALID_NULL = "Fields cannot be null";
    public static final String MSG_INVALID_INCORRECT_LOGIN = "Incorrect login";
    public static final String MSG_INVALID_INCORRECT_PASSWORD = "Incorrect password";
    public static final String MSG_INVALID_CONFIRM_PASSWORD_MISMATCH = "Password mismatch";
    public static final String MSG_INVALID_INCORRECT_EMAIL = "Incorrect e-mail";
    public static final String MSG_INVALID_INCORRECT_NUMBER = "Incorrect number";
    public static final String MSG_INVALID_COMPANY_EXCEEDED_CHARACTERS = "Exceeded number of characters";
    public static final String MSG_INVALID_CREDENTIALS = "Invalid credentials";

}
