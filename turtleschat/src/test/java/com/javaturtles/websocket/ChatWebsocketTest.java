package com.javaturtles.websocket;

import com.google.gson.Gson;
import com.javaturtles.cache.UserSessionCache;
import com.javaturtles.context.config.ServerConfiguration;
import com.javaturtles.dto.UserMessage;
import com.javaturtles.exception.NoSuchTopicException;
import com.javaturtles.factory.MessageHandlerFactory;
import com.javaturtles.util.TimeUtil;
import com.javaturtles.util.message_handler.MessageHandler;
import com.javaturtles.util.session_handler.ChatSessionsHandler;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.eclipse.jetty.websocket.api.UpgradeRequest;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.lang.reflect.Field;

import static com.javaturtles.testdata.ChatWebSocketTestData.*;

public class ChatWebsocketTest {
    private static final ChatSessionsHandler chatSessionsHandler = Mockito.mock(ChatSessionsHandler.class);
    private static final UserSessionCache userSessionCache = Mockito.mock(UserSessionCache.class);
    private static final MessageHandlerFactory messageHandlerFactory = Mockito.mock(MessageHandlerFactory.class);
    private static final Gson gson = new Gson();
    private static final Session session = Mockito.mock(Session.class);
    private static final WebSocketServletFactory webSocketServletFactory = Mockito.mock(WebSocketServletFactory.class);
    private static final UpgradeRequest upgradeRequest = Mockito.mock(UpgradeRequest.class);
    private static final MessageHandler messageHandler = Mockito.mock(MessageHandler.class);

    private static ChatWebsocket cut;

    @BeforeAll
    static void setUp() {
        try (MockedStatic<ServerConfiguration> serverConfigurationMockedStatic =
                     Mockito.mockStatic(ServerConfiguration.class)) {
            serverConfigurationMockedStatic.when(ServerConfiguration::getGson).thenReturn(gson);
            serverConfigurationMockedStatic.when(ServerConfiguration::getChatSessionsHandler)
                    .thenReturn(chatSessionsHandler);
            serverConfigurationMockedStatic.when(ServerConfiguration::getMessageHandlerFactory)
                    .thenReturn(messageHandlerFactory);
            serverConfigurationMockedStatic.when(ServerConfiguration::getUserSessionCache).thenReturn(userSessionCache);
            
            cut = new ChatWebsocket();
        }

        try {
            Field sessionField = cut.getClass().getDeclaredField(LOGIN);
            sessionField.setAccessible(true);
            sessionField.set(cut, CERTAIN_LOGIN);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void onConnectTest() {
        Mockito.when(session.getUpgradeRequest()).thenReturn(upgradeRequest);
        Mockito.when(upgradeRequest.getCookies()).thenReturn(HTTP_COOKIE_LIST);

        cut.onConnect(session);

        Mockito.verify(chatSessionsHandler, Mockito.times(NOTIFY_CONNECTED_TIMES)).notifyUserConnected(CERTAIN_LOGIN);
        Mockito.verify(chatSessionsHandler, Mockito.times(SEND_USERS_LIST_TIMES)).sendUsersList(CERTAIN_LOGIN);
        Mockito.verify(chatSessionsHandler, Mockito.times(SEND_ALL_CHAT_HISTORY_TIMES)).sendAllChatHistory(CERTAIN_LOGIN);
        Mockito.verify(session, Mockito.times(1)).setIdleTimeout(Long.MAX_VALUE);
        Mockito.verify(userSessionCache, Mockito.times(1)).addSession(CERTAIN_LOGIN, session);
    }

    static Arguments[] onTextMessageTestArgs() {
        return new Arguments[] {
                Arguments.arguments(MESSAGE_1, TOPIC_1, PAYLOAD_1, PROCESS_TIMES),
                Arguments.arguments(MESSAGE_2, TOPIC_2, PAYLOAD_2, PROCESS_TIMES),
                Arguments.arguments(MESSAGE_3, TOPIC_3, PAYLOAD_3, PROCESS_TIMES),
                Arguments.arguments(MESSAGE_4, TOPIC_4, PAYLOAD_4, PROCESS_TIMES),
        };
    }

    @ParameterizedTest
    @MethodSource("onTextMessageTestArgs")
    void onTextMessageTest(String message, String topic, String payload, int processTimes) throws NoSuchTopicException {
        Mockito.when(messageHandlerFactory.getUserMessageHandler(topic)).thenReturn(messageHandler);

        cut.onTextMessage(message);

        Mockito.verify(messageHandler, Mockito.times(processTimes)).process(payload, CERTAIN_LOGIN);
    }

    @Test
    void onCloseTest() {
        cut.onClose(StatusCode.SERVER_ERROR, ERROR_REASON);

        Mockito.verify(chatSessionsHandler, Mockito.times(NOTIFY_DISCONNECTED_TIMES)).notifyUserDisconnected(CERTAIN_LOGIN);
        Mockito.verify(userSessionCache, Mockito.times(REMOVE_SESSION_TIMES)).removeSession(CERTAIN_LOGIN);
    }

    @Test
    void configureTest() {
        cut.configure(webSocketServletFactory);

        Mockito.verify(webSocketServletFactory, Mockito.times(REGISTER_SOCKET_TIMES)).register(ChatWebsocket.class);
    }
}
